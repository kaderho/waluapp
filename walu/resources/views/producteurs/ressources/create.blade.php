@extends('templates.user')

{{-- description --}}
@section('meta-description', $description = "")

{{-- titre de la page --}}
@section('meta-title', $title = 'Ajout de Nouvelles ressources')

@section('css')
<style>
    
</style>
@endsection
@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-body">
        <form action="" method="POST" enctype="multipart/form-data">
            @csrf
           
            @include('layouts.form-ressource')
            
            <div class="modal-footer">
                <a class="btn btn-secondary" href="{{ route('ressource') }}">Retour</a>
                <button type="submit" class="btn btn-success">Ajouter</button>
            </div>
        </form>
    </div>
</div>

@endsection
@section('js')

@endsection