@extends('templates.user')

{{-- description --}}
@section('meta-description', $description = "")

{{-- titre de la page --}}
@section('meta-title', $title = "Mes ressources ")

@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3 d-flex justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">La liste de mes ressources </h6>

        @if (auth()->user()->isProducteur())
        <div class="dropdown">
            <a href="{{ route('ressource.create') }}" class="btn btn-sm btn-primary">
                <i class="fa fa-plus"></i> Nouvelle ressources
            </a>
        </div>
        @endif
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Libellé</th>
                        <th>Quantité</th>
                        <th>Montant</th>
                        <th>Preuve</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Libellé</th>
                        <th>Quantité</th>
                        <th>Montant</th>
                        <th>Preuve</th>
                    </tr>
                </tfoot>
                <tbody>
                    @forelse ($ressources as $ressource)
                    <tr>
                        <td>{{ $ressource->id }}</td>
                        <td>{{ $ressource->libelle }}</td>
                        <td>{{ $ressource->quantite }}</td>
                        <td>{{ $ressource->montant }}</td>
                        <td><a href="">{{ $ressource->preuve_url }}</a></td>
                        <td>
                            <a class="btn btn-sm btn-info" href="{{ route('ressource.edit', ['slug'=>$ressource->slug]) }}">
                                <i class="fa fa-edit"></i> Modifier</a>
                            <form action="{{ route('ressource.delete', ['slug'=>$ressource->slug]) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-sm btn-danger"
                                    href="">
                                    <i class="fa fa-trash"></i> Supprimer
                                </button>
                            </form>
                        </td>
                    </tr>
                    @empty
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <strong> Oups !!! </strong> Je n'ai trouvé aucune ressource !
                    </div>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
@section('js')

<script>
    $('#delProduit').click(function(){
        
        alert(this.data-slug);
    });
</script>
@endsection