@extends('templates.user')

{{-- description --}}
@section('meta-description', $description = "")

{{-- titre de la page --}}
@section('meta-title', $title = "Mes productions ". $status)

@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3 d-flex justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">La liste de mes productions {{ $status }} </h6>

        @if (auth()->user()->isProducteur())
        <div class="dropdown">
            <a href="{{ route('production.create') }}" class="btn btn-sm btn-primary">
                <i class="fa fa-plus"></i> Nouvelle production
            </a>
        </div>
        @endif
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Production</th>
                        <th>Quantité</th>
                        <th>Prix unitaire</th>
                        <th>Prix total</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                            <th>#</th>
                            <th>Production</th>
                            <th>Quantité</th>
                            <th>Prix unitaire</th>
                            <th>Prix total</th>
                    </tr>
                </tfoot>
                <tbody>
                    @forelse ($productions as $production)
                    <tr>
                        <td>{{ $production->id }}</td>
                        <td>{{ $production->nom_production }}</td>
                        <td>{{ $production->quantite }}</td>
                        <td>{{ $production->prix }}</td>
                        <td>{{ $production->prix *  $production->quantite }}</td>
                        <td><a class="btn btn-sm btn-primary" href="">
                            <i class="fa fa-eye"></i> Détails</a></td>
                    </tr>
                    @empty
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <strong> Oups !!! </strong> Je n'ai trouvé aucune production {{ $status }} !
                    </div>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
@section('js')

<script>
    $('#delProduit').click(function(){
        
        alert(this.data-slug);
    });
</script>
@endsection