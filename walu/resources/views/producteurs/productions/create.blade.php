@extends('templates.user')

{{-- description --}}
@section('meta-description', $description = "")

{{-- titre de la page --}}
@section('meta-title', $title = 'Nouvelle production')

@section('css')
{{-- contenu css --}}
@endsection
@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-body">
        <form action="" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                               
                                <input type="text" class="form-control" name="nom_production" id="nom"
                                    placeholder="Nom de la production">

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                                <select class="form-control" name="produit_id" id="">
                                        <option value="">Selectionnez le produit</option>
                                        @foreach ($produits as $produit)
                                        <option value="{{ $produit->id }}">{{$produit->designation}}</option>
                                        @endforeach
                                    </select>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <input type="number" class="form-control" name="quantite" id="nom"
                                    placeholder="Quantité de la production">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input type="number" class="form-control" name="prix" id="nom"
                                    placeholder="Prix par kilogramme de la production">
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input type="file" class="form-control" name="image" id="date_fin">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>
                <button type="submit" class="btn btn-success">Ajouter</button>
            </div>
        </form>
    </div>
</div>
@endsection
@section('js')
{{-- cotenu js --}}
@endsection