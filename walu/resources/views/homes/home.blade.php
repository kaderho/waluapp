@extends('templates.home')

@section('meta-description', $description = "")
@section('meta-title', $title = "")

@section('css')

@endsection
@section('content')
<div class="slider-area">
    <div class="slider-wrapper owl-carousel">
        <div class="slider-item home-one-slider-otem slider-item-four slider-bg-one">
            <div class="container">
                <div class="row">
                    <div class="slider-content-area">
                        <div class="slide-text">
                            <h1 class="homepage-three-title">Bienvenue sur <span>{{ config('app.name') }}</span>
                                Services</h1>
                            <h2>
                                Avec {{ config('app.name') }}, vous trouverez tous les produits de la campagne.
                                Des produits agricoles, de pêches, de l'élevage authentiques.
                                PLus de soucis pour investir dans le monde rural.
                            </h2>
                            {{-- <div class="slider-content-btn">
                                <section class="search-sec">
                                    <div class="container">
                                        <form action="#" method="post" class="search-form" novalidate="novalidate">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-12 p-0 form-group">
                                                            <input type="text" id="qui"
                                                                class="form-control search-input input-lg"
                                                                placeholder="Qui ?" autocomplete="off">
                                                            <div class="search-result"></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 p-0 form-group">
                                                            <input type="text" id="quoi"
                                                                class="form-control search-input1 input-lg"
                                                                placeholder="Quoi ?" autocomplete="off">
                                                            <div class="result-search"></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 p-0 form-group">
                                                            <input type="text" id="ou"
                                                                class="form-control search-input2 input-lg"
                                                                placeholder="Où ?" autocomplete="off">
                                                            <div class="result-ou"></div>
                                                        </div>
                                                        <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                                                            <button type="button" class="btn wrn-btn btn-lg"
                                                                style="background: #fd6802; color:#fff">
                                                                <i class="fa fa-search"></i> Rechercher
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </section>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="services" class="parallax section lb">
    <div class="container">
        <div class="section-title text-center">
            <h3>Nos Producteurs VIP</h3>
            <p class="lead"></p>
        </div>
        <!-- end title -->

        <div class="owl-services owl-carousel owl-theme">
           
            @forelse ($users as $producteur)
               @if ($producteur->profil == null)
                   {{-- <p>rien</p> --}}
               @endif

                @if ($producteur->profil != null)
                    <div class="service-widget">
                        <div class="post-media wow fadeIn">
                            <a href="" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="fa fa-eye"></i></a>
                            <img src="{{asset('storage/'.$producteur->profil->avatar)}}"
                                alt="" class="img-responsive img-rounded">
                        </div>
                        <div class="service-dit single-how-works">
                            <h3>{{$producteur->name}}</h3>
                            <h5>Activité : {{$producteur->role->nomRole}}</h5>
                            <p>{{$producteur->profil->description}}</p>
                            <a data-scroll href="" class="btn btn-info  ">Voir ma vitrine</a>
                    
                        </div>
                    </div>
                @endif
             
            @empty
                
            @endforelse
            <!-- end service -->

            {{-- <div class="service-widget">
                <div class="post-media wow fadeIn">
                    <a href="" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i
                            class="fa fa-eye"></i></a>
                    <img src="{{asset('user/images/eleveur.jpg')}}" alt="" class="img-responsive img-rounded">
                </div>
                <div class="service-dit single-how-works">
                    <h3>Oumar Gueye</h3>
                    <p>Un jeune Eleveur sénégalais 30 ans
                        sollicitudin. Proin nisi est, convallis nec purus vitae,convallis nec purus </p>
                    <a data-scroll href="projets.html" class="btn btn-info  ">Voir ma vitrine</a>

                </div>
            </div>
            <!-- end service -->

            <div class="service-widget">
                <div class="post-media wow fadeIn">
                    <a href="" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i
                            class="fa fa-eye"></i></a>
                    <img src="{{asset('user/images/pecheur.png')}}" alt="" class="img-responsive img-rounded">
                </div>
                <div class="service-dit single-how-works">
                    <h3>Samba Diallo</h3>
                    <p>Un jeune Pecheur sénégalais 30 ans
                        sollicitudin. Proin nisi est, convallis nec purus vitae, iaculis posuere sapien.</p>
                    <a data-scroll href="projets.html" class="btn btn-info  ">Voir ma vitrine</a>
                </div>
            </div>
            <!-- end service -->

            <div class="service-widget">
                <div class="post-media wow fadeIn">
                    <a href="" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i
                            class="fa fa-eye"></i></a>
                    <img src="{{asset('user/images/pecheur.png')}}" alt="" class="img-responsive img-rounded">
                </div>
                <div class="service-dit single-how-works">
                    <h3>Samba Diallo</h3>
                    <p>Un jeune agriculteur sénégalais 30 ans
                        sollicitudin. Proin nisi est, Cum sociis natoque
                        convallis nec </p>
                    <a data-scroll href="projets.html" class="btn btn-info  ">Voir ma vitrine</a>

                </div>
            </div>
            <!-- end service --> --}}
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>

<div id="features" class="section lb">
    <div class="container">
        <div class="section-title text-center">
            <h3>Notre Application Mobille</h3>
            <p class="lead">Bientôt disponible<br></p>
        </div>
        <!-- end title -->

        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <ul class="features-left">
                    <li class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.2s">
                        <i class="flaticon-wordpress-logo"></i>
                        <div class="fl-inner">
                            <h4>Agriculture</h4>
                            <p></p>
                        </div>
                    </li>
                    <li class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.3s">
                        <i class="flaticon-windows"></i>
                        <div class="fl-inner">
                            <h4>Elevage</h4>
                            <p></p>
                        </div>
                    </li>
                    <li class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.4s">
                        <i class="flaticon-price-tag"></i>
                        <div class="fl-inner">
                            <h4>Peche</h4>
                            <p></p>
                        </div>
                    </li>
                    <li class="wow fadeInLeft" data-wow-duration="1s" data-wow-delay="0.5s">
                        <i class="flaticon-new-file"></i>
                        <div class="fl-inner">
                            <h4>Autres</h4>
                            <p></p>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 hidden-xs hidden-sm">
                <img src="{{asset('user/images/ipad.png')}}" class="img-center img-responsive" alt="">
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <ul class="features-right">
                    <li class="wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.2s">
                        <i class="flaticon-pantone"></i>
                        <div class="fr-inner">
                            <h4>Agriculture</h4>
                            <p></p>
                        </div>
                    </li>
                    <li class="wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.3s">
                        <i class="flaticon-cloud-computing"></i>
                        <div class="fr-inner">
                            <h4>Elevage</h4>
                            <p></p>
                        </div>
                    </li>
                    <li class="wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.4s">
                        <i class="flaticon-line-graph"></i>
                        <div class="fr-inner">
                            <h4>Pêche</h4>
                            <p></p>
                        </div>
                    </li>
                    <li class="wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
                        <i class="flaticon-coding"></i>
                        <div class="fr-inner">
                            <h4>Autres</h4>
                            <p></p>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- end col -->
        </div>
        <!-- end row -->
    </div>
    <!-- end container -->
</div>
<!-- end section -->

<div id="services" class="parallax section lb">
    <div class="container">
        <div class="section-title text-center">
            <h3>Nos services</h3>
            <p class="lead"></p>
        </div><!-- end title -->
        <div class="row text-center">
            <div class="col-md-3 col-sm-6">
                <div class="about-item">
                    <div class="about-icon">
                        <span class="icon icon-cart"></span>
                    </div>
                    <div class="about-text">
                        <h3> <a href="#">Vente</a></h3>
                        <p></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="about-item">
                    <div class="about-icon">
                        <span class="icon icon-database"></span>
                    </div>
                    <div class="about-text">
                        <h3> <a href="#">Gestion de production</a></h3>
                        <p></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="about-item">
                    <div class="about-icon">
                        <span class="icon icon-eye"></span>
                    </div>
                    <div class="about-text">
                        <h3> <a href="#">Publicité</a></h3>
                        <p></p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="about-item">
                    <div class="about-icon">
                        <span class="icon icon-cloud"></span>
                    </div>
                    <div class="about-text">
                        <h3> <a href="#">Rélations clients</a></h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
    </div><!-- end container -->
</div>

<div id="services" class="parallax section lb">
    <div class="container">
        <div class="section-title text-center">
            <h3>Comment ça marche ?</h3>
            <p class="lead"></p>
        </div><!-- end title -->

        <div class="row text-center">

        </div>
    </div><!-- end container -->
</div>

<div id="services" class="parallax section lb">
    <div class="container">
        <div class="section-title text-center">
            <h3>Nos témoignages</h3>
            <p class="lead"></p>
        </div><!-- end title -->
    </div><!-- end container -->
</div>


@endsection
@section('js')
<script src=" {{asset('dist/js/search.js')}} "></script>
@endsection