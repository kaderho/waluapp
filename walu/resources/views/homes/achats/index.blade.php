@extends('templates.produit')

@section('content')


   @include('layouts.produit.header')
    
   @include('layouts.produit.service')
    
   @include('layouts.produit.categorie')
    
    @include('layouts.produit.produit')
    
    {{-- @include('layouts.produit.deal') --}}
@endsection