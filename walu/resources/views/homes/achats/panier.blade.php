@extends('templates.produit')
@section('css')
<style>
    .btn-cart {
        background: #fd6802;
        color: #fff
    }

    .btn-cart:hover {
        background: #fff;
        color: fd6802;
        border: solid 1px #fd6802
    }

    .btn-cart-r {
        background: red;
        color: #fff
    }

    .btn-cart-r:hover {
        background: #fff;
        color: red;
        border: solid 1px red
    }
</style>
@endsection
@section('content')
<div class="hero-wrap hero-bread" style="background-image: url({{asset('produits/images/panier.jpg')}});">
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 col-sm-9 col-xs-12 ftco-animate text-center" style="background: rgba(0, 0, 0, 0.3)">
                <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Accueil</a></span> <span class="mr-2"><a
                            href="index.html">Panier</a></span></p>
                <h1 class="mb-0 bread"> Mon panier </h1>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section ftco-cart">
    @if (Cart::count() > 0)
    <div class="container">
        <div class="row">
            <div class="col-md-12 ftco-animate">
                <div class="cart-list">
                    <table class="table">
                        <thead class="thead-primary">
                            <tr class="text-center">
                                <th>&nbsp;</th>
                                <th>Nom du produit</th>
                                <th>Prix</th>
                                <th>Quantité</th>
                                <th>Total</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach (Cart::content() as $production)
                            <tr class="text-center">
                                <td class="image-prod">
                                    <div class="img"
                                        style="background-image:url( {{$production->model->url_image != null ? asset('storage/'.$production->model->url_image) : asset('storage/'.$production->model->produit->url_image)}})">
                                    </div>
                                   
                                </td>

                                <td class="product-name">
                                    <h3> {{ $production->model->produit->designation }} </h3>
                                    <p> Description </p>
                                </td>

                                <td class="price"> {{ $production->price }} <span>FCFA</span></td>

                                <td class="quantity">
                                    <div class="input-group mb-3">
                                        <input type="number" name="quantity" class="quantity form-control input-number"
                                            value="1" min="1" max="100"
                                            id ="{{ $production->name }} " data-id="{{ $production->rowId }}" 
                                            value="{{ $production->qty }}">
                                    </div>
                                </td>

                                <td class="total">{{Cart::total() }} <span>FCFA</span></td>
                                <td>
                                    <form action="{{ route('panier.destroy', ['rowId' => $production->rowId]) }}" method="POST">
                                        @csrf
                                       @method('DELETE')
                                        <button type="submit" class="form-control btn btn-danger btn-sm"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr><!-- END TR-->
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="d-flex justify-content-between mt-3">
                    <a href="{{ route('achat') }}" class="btn btn-lg btn-cart">Continuer vos achats</a>
                    <a href="{{ route('panier.vider') }}" class="btn btn-lg btn-cart-r">Vider le panier</a>

                </div>
            </div>
        </div>
        <div class="row justify-content-end">
            {{-- <div class="col-lg-4 mt-5 cart-wrap ftco-animate">
                    <div class="cart-total mb-3">
                        <h3>Coupon Code</h3>
                        <p>Enter your coupon code if you have one</p>
                        <form action="#" class="info">
                            <div class="form-group">
                                <label for="">Coupon code</label>
                                <input type="text" class="form-control text-left px-3" placeholder="">
                            </div>
                        </form>
                    </div>
                    <p><a href="checkout.html" class="btn btn-primary py-3 px-4">Apply Coupon</a></p>
                </div>
                <div class="col-lg-4 mt-5 cart-wrap ftco-animate">
                    <div class="cart-total mb-3">
                        <h3>Estimate shipping and tax</h3>
                        <p>Enter your destination to get a shipping estimate</p>
                        <form action="#" class="info">
                            <div class="form-group">
                                <label for="">Country</label>
                                <input type="text" class="form-control text-left px-3" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="country">State/Province</label>
                                <input type="text" class="form-control text-left px-3" placeholder="">
                            </div>
                            <div class="form-group">
                                <label for="country">Zip/Postal Code</label>
                                <input type="text" class="form-control text-left px-3" placeholder="">
                            </div>
                        </form>
                    </div>
                    <p><a href="checkout.html" class="btn btn-primary py-3 px-4">Estimate</a></p>
                </div> --}}
            <div class="col-lg-6 mt-5 cart-wrap ftco-animate">
                <div class="cart-total mb-3">
                    <h3>Totaux du panier</h3>
                    <p class="d-flex">
                        <span>Sous total</span>
                        <span>{{Cart::subtotal() }} FCFA</span>
                    </p>
                    <p class="d-flex">
                        <span>Livraison</span>
                        <span>0.00 FCFA</span>
                    </p>
                    <p class="d-flex">
                        <span>Reduction</span>
                        <span>{{Cart::discount() }} FCFA</span>
                    </p>
                    <hr>
                    <p class="d-flex total-price">
                        <span>Total</span>
                        <span>{{Cart::total() }} FCFA</span>
                    </p>
                </div>
                <p><a href="checkout.html" class="btn btn-primary py-3 px-4">Passer la commande</a></p>
            </div>
        </div>
    </div>
    @else
    <div class="container">
        <div class="alert alert-info alert-dismissible" role="alert">
            <p><strong> Hey !!! </strong> Il n'y aucun produit dans votre panier.</p>
            <p>Vous manquez d'inspiration ? <a href="{{ route('achat') }}">cliquez ici</a> </p>
        </div>
    </div>
    @endif


</section>
@endsection

@section('js')
<script>
    $(document).ready(function(){

		var quantitiy=0;
		   $('.quantity-right-plus').click(function(e){
		        
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		            
		            $('#quantity').val(quantity + 1);

		          
		            // Increment
		        
		    });

		     $('.quantity-left-minus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		      
		            // Increment
		            if(quantity>0){
		            $('#quantity').val(quantity - 1);
		            }
		    }); 

             //const classname = $(".quantity");
    $(".quantity").each(function(){
        $(this).change(function() {
            const id = $(this).attr('data-id');
    
           //alert(id);
            axios.patch(`/panier/${id}/mis-a-jour`, {
                quantity: this.value
            })
            
            .then(function (response) {
               console.log(response);
            //    window.location.href = '{{ route('panier') }}';
            })
            
            .catch(function (error) {
                console.log(error);
    
            //    window.location.href = '{{ route('panier') }}';
            });
        });
        });
		}); //fin document


</script>
@endsection