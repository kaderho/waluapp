@extends('templates.produit')

@section('css')
    <style>
        .btn-cart{background: #fd6802; color: #fff}
        .btn-cart:hover{background: #fff; color: fd6802; border: solid 1px #fd6802}
    </style>
@endsection
@section('content')
<div class="hero-wrap hero-bread" style="background-image: url({{asset($production->produit->secteur->url_image)}});">
    <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
            <div class="col-md-9 col-sm-9 col-xs-12 ftco-animate text-center" style="background: rgba(0, 0, 0, 0.3)">
                <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Accueil</a></span> <span class="mr-2"><a
                            href="index.html">Produit</a></span> <span> {{ $production->produit->secteur->nom_secteur }}
                    </span></p>
                <h1 class="mb-0 bread"> {{ $production->nom_production }} </h1>
            </div>
        </div>
    </div>
</div>

<section class="ftco-section">
    <form action="{{ route('panier.store') }}" method="post">
        @csrf
        <div class="container">
            <div class="row">
                <div class="col-lg-6 mb-5 ftco-animate">
                    <a href="images/product-1.jpg" class="image-popup"><img
                            src=" {{ $production->url_image != null ? asset('storage/'.$production->url_image) : asset($production->produit->url_image) }} "
                            class="img-fluid" alt="Colorlib Template"></a>
                </div>
                <div class="col-lg-6 product-details pl-md-5 ftco-animate">
                    <h3> {{  $production->produit->designation }} </h3>
                    <div class="rating d-flex">
                        <p class="text-left mr-4">
                            <a href="#" class="mr-2">5.0</a>
                            <a href="#"><span class="fa fa-star"></span></a>
                            <a href="#"><span class="fa fa-star"></span></a>
                            <a href="#"><span class="fa fa-star"></span></a>
                            <a href="#"><span class="fa fa-star"></span></a>
                            <a href="#"><span class="fa fa-star"></span></a>
                        </p>
                        <p class="text-left mr-4">
                            <a href="#" class="mr-2" style="color: #000;">100 <span
                                    style="color: #bbb;">Evalution</span></a>
                        </p>
                        <p class="text-left">
                            <a href="#" class="mr-2" style="color: #000;">500 <span
                                    style="color: #bbb;">Vendu</span></a>
                        </p>
                    </div>
                    <p class="price"><span> {{ $production->prix * 1.049 }} <span>FCFA</span></span></p>
                    <p>
                        description du produit <br>

                        <span style="color: #fd6802"> Producteur : <strong> {{ $production->user->name }} </strong></span>
                    </p>
                    <div class="row mt-4">
                        <div class="col-md-6">
                            <div class="form-group d-flex">
                                <div class="select-wrap">
                                    <div class="icon"><span class="fa fa-sort-down"></span></div>
                                    <select name="taille" id="" class="form-control">
                                        <option value="Petit">Petit</option>
                                        <option value="Moyen">Moyen</option>
                                        <option value="Grand">Grand</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="w-100"></div>
                        <div class="input-group col-md-6 d-flex mb-3">
                            <span class="input-group-btn mr-2">
                                <button type="button" class="quantity-left-minus btn" data-type="minus" data-field="">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </span>
                            <input type="text" id="quantity" name="poids" class="form-control input-number" value="1"
                                min="1" max="100">
                            <span class="input-group-btn ml-2">
                                <button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
                                    <i class="fa fa-plus"></i>
                                </button>
                            </span>
                        </div>
                        <div class="w-100"></div>
                        <div class="col-md-12">
                            <p style="color: green;">{{ $production->quantite }} kg disponible</p>
                        </div>
                    </div>

                        <input type="text" hidden="" name="id" value="{{ $production->id }}">
                        <input type="text" hidden="" name="nom" value="{{ $production->produit->designation }}">
                        <input type="text" hidden="" name="prix" value="{{ $production->prix }}">

                        <p> <input type="submit" id="addCart" value="Ajouter au parnier" class="btn btn-lg btn-cart">

                    </p>
                </div>
            </div>
        </div>
    </form>
</section>

<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-3 pb-3">
            <div class="col-md-12 heading-section text-center ftco-animate">
                <span class="subheading">Produits</span>
                <h2 class="mb-4">Produits semblables</h2>
                <p>Nous avons encore plus de produits agricoles à vous présenter</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @forelse ($productions as $production)
            <div class="col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="#" class="img-prod"><img class="img-fluid"
                            src=" {{ asset('produits/images/product-2.jpg') }} " alt="Colorlib Template">
                        <div class="overlay"></div>
                    </a>
                    <div class="text py-3 pb-4 px-3 text-center">
                        <h3><a href="#"> {{ $production->produit->designation }} </a></h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span> {{ $production->prix }} <span>FCFA</span></span></p>
                            </div>
                        </div>
                        <div class="bottom-area d-flex px-3">
                            <div class="m-auto d-flex">
                                <a href="{{ route('achat.produit.show', ['slug' => $production->slug]) }}"
                                    class="add-to-cart d-flex justify-content-center align-items-center text-center">
                                    <span><i class="fa fa-eye"></i></span>
                                </a>
                                {{-- <a href="#" class="buy-now d-flex justify-content-center align-items-center mx-1">
                                    <span><i class="fa fa-cart"></i></span>
                                </a> --}}
                                {{-- <a href="#" class="heart d-flex justify-content-center align-items-center ">
                                    <span><i class="fa fa-heart"></i></span>
                                </a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @empty

            @endforelse
        </div>
    </div>
</section>
@endsection

@section('js')
<script>
    $(document).ready(function(){

		var quantitiy=0;
		   $('.quantity-right-plus').click(function(e){
		        
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		            
		            $('#quantity').val(quantity + 1);

		          
		            // Increment
		        
		    });

		     $('.quantity-left-minus').click(function(e){
		        // Stop acting like a button
		        e.preventDefault();
		        // Get the field name
		        var quantity = parseInt($('#quantity').val());
		        
		        // If is not undefined
		      
		            // Increment
		            if(quantity>0){
		            $('#quantity').val(quantity - 1);
		            }
		    });
		    
        });

//         $('#addCart').click(function(){
//             var path = location.pathname;
//             axios.post('panier/ajouter', {
//                 id: $('input[name="id"]').val(),
//                 nom: $('input[name="nom"]').val(),
//                 prix : $('input[name="prix"]').val(),
//                 poids : $('input[name="poids"]').val(),
//                 taille : $('input[name="taille"]').val(),
//   })
//   .then(function (response) {
//     console.log(response);
//     // window.location.href = path;
//   })
//   .catch(function (error) {
//     console.log(error);
//     // window.location.href = path;
//   });
//         });
        
        //const classname = $(".quantity");
    $(".quantity").each(function(){
        $(this).change(function() {
            const id = $(this).attr('data-id');
    
           //  alert(id);
            axios.patch(`/panier/${id}`, {
                quantity: this.value
            })
            
            .then(function (response) {
               //console.log(response);
               window.location.href = '{{ route('panier') }}';
            })
            
            .catch(function (error) {
                //console.log(error);
    
               window.location.href = '{{ route('panier') }}';
            });
        });
    });
</script>
@endsection