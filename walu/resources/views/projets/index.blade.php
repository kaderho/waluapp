@extends('templates.user')

{{-- description --}}
@section('meta-description', $description = "")

{{-- titre de la page --}}
@section('meta-title', $title = "Nos projet ". $status)

@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3 d-flex justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">La liste de tous nos {{ $status }} </h6>

        @if (auth()->user()->isProducteur())
        <div class="dropdown">
            <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-plus"></i>
                Nouveau
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="{{ route('projet.create') }}">
                    <i class="fa fa-cube fa-sm fa-fw mr-2 text-gray-400"></i>
                    Projet
                </a>
            </div>
        </div>
        @endif
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Titre du projet</th>
                        <th>Secteur</th>
                        <th>Propriétaire</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>#</th>
                        <th>Titre du projet</th>
                        <th>Secteur</th>
                        <th>Propriétaire</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
                <tbody>
                    @forelse ($projets as $projet)
                    <tr>
                        <td>{{ $projet->id }}</td>
                        <td>{{ $projet->nom_projet }}</td>
                        <td>{{ $projet->user->role->nomRole }}</td>
                        <td>{{ $projet->user->name }}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{ route('projet.show.a', [$projet->code]) }}">
                                <i class="fa fa-eye"></i> Détails</a>
                            @if (auth()->user()->isProducteur())
                            <a class="btn btn-sm btn-info" href="{{ route('projet.show.a', [$projet->code]) }}">
                                <i class="fa fa-edit"></i> Modifier</a>

                            <form action="{{ route('projet.destroy', [$projet->slug]) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-sm btn-danger"
                                    href="{{ route('projet.show.a', [$projet->code]) }}">
                                    <i class="fa fa-trash"></i> Supprimer
                                </button>
                            </form>

                            @endif
                        </td>
                    </tr>
                    @empty
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <strong> Oups !!! </strong> Je n'ai trouvé aucun {{ $status }} !
                    </div>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
@section('js')

<script>
    $('#delProduit').click(function(){
        
        alert(this.data-slug);
    });
</script>
@endsection