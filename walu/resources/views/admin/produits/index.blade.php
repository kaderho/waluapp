@extends('templates.user')

{{-- description --}}
@section('meta-description', $description = "")

{{-- titre de la page --}}
@section('meta-title', $title = "Nos produits")

@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3 d-flex justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">La liste de tous nos produits</h6>
        <div class="dropdown">
            <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-plus"></i>
                Nouveau
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
               <a class="dropdown-item" href="#" data-toggle="modal" data-target="#addProduit">
                    <i class="fa fa-cube fa-sm fa-fw mr-2 text-gray-400"></i>
                    Produit
                </a>
               <a class="dropdown-item" href="#" data-toggle="modal" data-target="#addCatProd">
                    <i class="fa fa-cube fa-sm fa-fw mr-2 text-gray-400"></i>
                    Catégorie produit
                </a>
                <a class="dropdown-item" href="#"></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>Image</th>
                        <th>Catégorie</th>
                        <th>Désignation</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Image</th>
                        <th>Catégorie</th>
                        <th>Désignation</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
                <tbody>
                    @forelse ($produits as $produit)
                    <tr>
                        <td><img width="150px" height="100px" src="
                            {{strpos($produit->url_image, 'produit') !== false ?
                            asset($produit->url_image) :  asset('storage/'.$produit->url_image) }}" 
                            alt="" ></td>
                        <td>{{ $produit->categorieProduit->nom }}</td>
                        <td>{{ $produit->designation }}</td>
                        <td>
                            {{-- <a class="btn btn-sm btn-danger" data-slug="{{$produit->slug}}" href="{{ route('produit.destroy', [$produit->slug]) }}"
                                    href="#" data-toggle="modal" data-target="#delProduit">
                                <i class="fa fa-trash"></i> Supprimer</a> --}}

                                <form action="{{ route('produit.destroy', [$produit->slug]) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                    <a class="btn btn-sm btn-info" href="{{ route('produit.update', [$produit->slug]) }}">
                                        <i class="fa fa-edit"></i> Modifier</a>
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Voulez-vous vraiment suprimer ce produit ?')">
                                        <i class="fa fa-trash"></i> Supprimer
                                    </button>
                                </form>
                        </td>
                    </tr>
                    @empty
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <strong> Oups !!! </strong> Je n'ai trouvé aucun produit !
                    </div>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>

@include('layouts.user.modal-addProduit')
@include('layouts.user.modal-editProduit')

@include('layouts.user.modal-addCatProd')
@endsection
@section('js')

  <script>
        $('#delProduit').click(function(){
        
        alert(this.data-slug);
    });
  </script>
@endsection