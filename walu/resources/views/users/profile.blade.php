@extends('templates.user')

{{-- description --}}
@section('meta-description', $description = "Mes informations personnelles")

{{-- titre de la page --}}
@section('meta-title', $title = "Mon Proile ")

@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3 d-flex justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">{{ $description }} </h6>
    </div>
    <div class="card-body">
       <div class="container">
        <h1>Editer Profile</h1>
        <hr>
            <form class="" action="" method="POST" role="form" enctype="multipart/form-data">
                <div class="row">
                @csrf
                @method('PUT')
            <!-- left column -->
            <div class="col-md-3">
                <div class="text-center">
                    <img src="{{ $user->profil != null ? asset('storage/'.$user->profil->avatar) :'//placehold.it/100'}}" class="avatar img-thumbnail img-circle" alt="avatar">
                    <h6>Cette photo sera visible par tout le monde, elle pourrait attirer des clients</h6>
    
                    <input type="file" name="image" class="form-control">
                </div>
            </div>
    
           <div class="col-md-9">
               <!-- edit form column -->
                <div class="personal-info">
                   @if ($user->profil == null)
                       <div class="alert alert-danger alert-dismissable">
                            <a class="panel-close close" data-dismiss="alert">×</a>
                            <i class="fa fa-info"></i>
                            Ceci est un <strong>avertissement</strong>. Votre profile n'est pas complet !.
                        </div>
                   @endif
                    {{-- <h3>Personal info</h3> --}}
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="name">Prénom(s) et Nom</label>
                        <div class="col-lg-8">
                            <input class="form-control" name="name" id="name" type="text" value="{{ $user->name }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="adresse">Adresse</label>
                        <div class="col-lg-8">
                            <input class="form-control" type="text" name="adresse" id="adresse" value="{{ $user->adresse }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="telephone">Téléphone</label>
                        <div class="col-lg-8">
                            <input class="form-control" name="telephone" id="telephone" type="text" value="{{ $user->telephone }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label" for="email" >Email</label>
                        <div class="col-lg-8">
                            <input class="form-control" name="email" id="email" type="text" value="{{ $user->email }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-lg-3 control-label">Description détaillée</label>
                        <div class="col-lg-8">
                            <div class="ui-select">
                                <textarea class="form-control" name="description" id="description"
                                    placeholder="Decrivez vous ainsi que l'activité que vous faites avec plus de détails.
                                                Cette description sera visible par tout le monde.
                                                Plus il y a d'informations interessantes mieux sera votre réputation et votre vitrine sera visitée." cols="60" rows="10"></textarea>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="form-group">
                                        <label class="col-md-3 control-label">Username:</label>
                                        <div class="col-md-8">
                                            <input class="form-control" type="text" value="janeuser">
                                        </div>
                                    </div> --}}
                    <div class="form-group">
                        <label class="col-md-3 control-label">Nouveau mot de passe</label>
                        <div class="col-md-8">
                            <input class="form-control" type="password" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Confirmer mot de passe</label>
                        <div class="col-md-8">
                            <input class="form-control" type="password" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-8">
                            <input type="submit" class="btn btn-info" value="Enregistrer modification">
                            <span></span>
                            <input type="reset" class="btn btn-secondary" value="Annuler">
                        </div>
                    </div>
                    </form>
                </div>
           </div>
        </div>
    </div>
    <hr>
    </div>
</div>

@endsection
@section('js')

@endsection