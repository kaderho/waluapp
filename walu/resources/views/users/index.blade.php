@extends('templates.user')

{{-- description --}}
@section('meta-description', $description = "")

{{-- titre de la page --}}
@section('meta-title', $title = "Nos ". $status)

@section('content')
<!-- DataTales Example -->
<div class="card shadow mb-4">
    <div class="card-header py-3 d-flex justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">La liste de tous nos {{ $status }} </h6>
        <div class="dropdown">
            <button class="btn btn-primary btn-sm dropdown-toggle" type="button" id="dropdownMenuButton"
                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-plus"></i>
                Nouveau
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#addProduit">
                    <i class="fa fa-cube fa-sm fa-fw mr-2 text-gray-400"></i>
                    Agriculteur
                </a>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#addCatProd">
                    <i class="fa fa-cube fa-sm fa-fw mr-2 text-gray-400"></i>
                    Eleveur
                </a>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#addCatProd">
                    <i class="fa fa-cube fa-sm fa-fw mr-2 text-gray-400"></i>
                    Pecheur
                </a>
                <a class="dropdown-item" href="#"></a>
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th>code</th>
                        <th>Nom et Prénom(s)</th>
                        <th>Téléphone</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>code</th>
                        <th>Nom et Prénom(s)</th>
                        <th>Téléphone</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                </tfoot>
                <tbody>
                    @forelse ($users as $user)
                    <tr>
                        <td>{{ $user->code }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->telephone }}</td>
                        <td>{{ $user->email }}</td>
                        <td>
                            <a class="btn btn-sm btn-info" href="{{ route('user.show.a', [$user->code]) }}">
                                <i class="fa fa-edit"></i> Détails</a>
                        </td>
                    </tr>
                    @empty
                    <div class="alert alert-warning alert-dismissible" role="alert">
                        <strong> Oups !!! </strong> Je n'ai trouvé aucun {{$status}} !
                    </div>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>

@endsection
@section('js')

<script>
    $('#delProduit').click(function(){
        
        alert(this.data-slug);
    });
</script>
@endsection