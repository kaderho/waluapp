@component('mail::message')
# Bienvenu chez {{ config('app.name') }}

<p>Salut <b>{{ $user->name }}</b>, ci-dessous vous trouverez un bouton, pour la validation de votre
    compte.</p>
{{-- the attribute token permit to validate the user account --}}
@component('mail::button', ['url' => config('app.url'). "/validation/$user->code/$user->token"])
Valider mon compte
@endcomponent

Merci,<br>
{{ config('app.name') }}
@endcomponent