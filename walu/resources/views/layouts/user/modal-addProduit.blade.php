<!-- Logout Modal-->
<div class="modal fade" id="addProduit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Ajouter un nouveau produit</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form action="{{ route('produit.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <input type="text" class="form-control" required name="designation" id="designation"
                            placeholder="Nom du produit">
                    </div>
                    <div class="form-group">
                        <select name="categorie_produit_id" required class="form-control" id="'categorie_produit_id">
                            <option value="">Sélectionnez une catégorie...</option>
                            @foreach ($catProduits as $catProd)
                            <option value="{{ $catProd->id }}">{{ $catProd->nom }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                            <select name="secteur_id" required class="form-control" id="'categorie_produit_id">
                                <option value="">Sélectionnez le secteur...</option>
                                @foreach ($secteurs as $secteur)
                                <option value="{{ $secteur->id }}">{{ $secteur->nom_secteur }}</option>
                                @endforeach
                            </select>
                        </div>
                    <div class="form-group">
                        <input type="file" required class="form-control" name="image" id="designation" placeholder="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Annuler</button>
                    <button type="submit" class="btn btn-success">Ajouter</button>
                </div>
            </form>
        </div>
    </div>
</div>