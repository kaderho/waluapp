<!-- Sidebar -->
<ul class="navbar-nav sidebar sidebar-dark accordion" id="accordionSidebar" style="background: #fd6802">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('/') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3"> {{ config('app.name') }} </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item active">
        <a class="nav-link" href="{{ route('user', [auth()->user()->code]) }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Tableau de bord</span></a>
    </li>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseCmdes" aria-expanded="true"
            aria-controls="collapsePages">
            <i class="fas fa-fw fa-cart-plus"></i>
            <span>Commandes</span>
        </a>
        <div id="collapseCmdes" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Mes commandes</h6>
                <a class="collapse-item" href="login.html">Commandes en cours</a>
                <a class="collapse-item" href="login.html">Commandes livrées</a>
                <a class="collapse-item" href="register.html">Commandes futures</a>

                @if (auth()->user()->isProducteur())
                <h6 class="collapse-header">Mes commandes clients</h6>
                <a class="collapse-item" href="login.html">Commandes en cours</a>
                <a class="collapse-item" href="login.html">Commandes livrées</a>
                <a class="collapse-item" href="register.html">Commandes futures</a>
                @endif
            </div>
        </div>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">

    @if (auth()->user()->isOwners())

    <!-- Heading -->
    <div class="sidebar-heading">
        Utilisateurs
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
            aria-controls="collapseTwo">
            <i class="fa fa-users"></i>
            <span>Producteurs</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Nos utilisateurs:</h6>
                <a class="collapse-item" href="{{ route('user.list', ['user' => 'agriculteur']) }}">Agriculteurs</a>
                <a class="collapse-item" href="{{ route('user.list', ['user' => 'eleveur']) }}">Eleveurs</a>
                <a class="collapse-item" href="{{ route('user.list', ['user' => 'pecheur']) }}">Pêcheurs</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-users"></i>
            <span>Autres</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Nos utilisateurs:</h6>
                <a class="collapse-item" href="{{ route('user.list', ['user' => 'acheteur']) }}">Achêteurs</a>
                <a class="collapse-item" href="{{ route('user.list', ['user' => 'investisseur']) }}">Investisseurs</a>
                <a class="collapse-item" href="{{ route('user.list', ['user' => 'autre']) }}">Autres</a>
            </div>
        </div>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Heading -->
    <div class="sidebar-heading">
        Activités
    </div>

    <!-- Nav Item - Pages Collapse Menu -->

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseRessource" aria-expanded="true"
            aria-controls="collapseUtilities">
            <i class="fas fa-cubes"></i>
            <span>Ressources</span>
        </a>
        <div id="collapseRessource" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Nos ressources:</h6>
                <a class="collapse-item" href="{{ route('produit.index') }}">Produits</a>
                <a class="collapse-item" href="utilities-border.html">secteurs</a>
                <a class="collapse-item" href="utilities-animation.html">Autres</a>
            </div>
        </div>
    </li>

    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="charts.html">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Statistiques</span></a>
    </li>

    <!-- Nav Item - Tables -->
   
    @endif
    @if (auth()->user()->isProducteur())
    <!-- Heading -->
    <div class="sidebar-heading">
        Finances
    </div>

    <!-- Nav Item - Pages Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
            aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Activités</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Mes activités</h6>
                <a class="collapse-item" href="{{ route('ressource') }}">Mes ressources</a>
                <a class="collapse-item" href="">Mes ventes</a>
            </div>
        </div>
    </li>
    <!-- Nav Item - Utilities Collapse Menu -->
    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
            aria-expanded="true" aria-controls="collapseUtilities">
            <i class="fas fa-database"></i>
            <span>Productions</span>
        </a>
        <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities" data-parent="#accordionSidebar">
            <div class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Activités:</h6>
                <a class="collapse-item" href="{{ route('production', ['status' => 'en cours']) }}">Productions en cours</a>
                <a class="collapse-item" href="{{ route('production', ['status' => 'terminées']) }}">Productions terminés</a>
                <a class="collapse-item" href="utilities-animation.html">Mes produits</a>
                <a class="collapse-item" href="utilities-animation.html">Mes ventes</a>
            </div>
        </div>
    </li>
    <!-- Divider -->
    <hr class="sidebar-divider">
    @endif
    @if (auth()->user()->isProducteur())
    <!-- Heading -->
    <div class="sidebar-heading">
        Autres
    </div>
    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="charts.html">
            <i class="fas fa-fw fa-chart-area"></i>
            <span>Statistiques</span></a>
    </li>
    @endif

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->