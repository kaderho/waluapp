<nav class="megamenu navbar navbar-expand-lg ftco_navbar ftco-navbar-light" id="ftco-navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" style="color:green" href="{{ url('/') }}"> {{ config('app.name') }} </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
                aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-bars"></span> <span style="color:green">Menu</span>
            </button>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item"><a class=" {{ set_active('accueil') }} " href="{{ route('home') }}">Acueil</a></li>
                <li class="nav-item"><a class=" {{ set_active('produits') }} " href="{{ route('achat') }}">Nos
                        prodits</a></li>

                @if (auth()->check())
                <li class="nav-item"><a class=" {{ set_active('connexion') }} "
                        href="{{ route('user', [auth()->user()->code]) }}"><i class="fa fa-user"></i> Mon
                        compte</a></li>
                @else
                <li class="nav-item"><a class=" {{ set_active('connexion') }} "
                        href="{{ route('connexion') }}">Connexion</a></li>
                <li class="nav-item"><a class=" {{ set_active('inscription') }} "
                        href="{{ route('inscription') }}">Inscription</a></li>
                @endif

                <li class="nav-item">
                    <a href="{{ route('panier') }}">
                        <i class="fa fa-cart-plus"></i> [{{ Cart::count() }}]
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>