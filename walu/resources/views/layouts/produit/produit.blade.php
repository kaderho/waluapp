<section class="ftco-section">
    <div class="container">
        <div class="row justify-content-center mb-3 pb-3"  id="agicole">
            <div class="col-md-12 heading-section text-center ftco-animate">
                {{-- <span class="subheading">Produ Products</span> --}}
                <h2 class="mb-4">Nos Produits agricoles</h2>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @forelse ($productionsA as $production)
            <div class="col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="#" class="img-prod"><img class="img-fluid"
                            src=" 
                            {{$production->url_image != null ? asset('storage/'.$production->url_image) : asset($production->produit->url_image) }} "
                            alt="Colorlib Template">
                        <span class="status" style="background: green">Frais</span>
                        <div class="overlay"></div>
                    </a>
                    <div class="text py-3 pb-4 px-3 text-center">
                        <h3><a href="#"> {{ $production->produit->designation }} </a></h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price">
                                    {{-- <span class="mr-2 price-dc">$120.00</span> --}}
                                    <span class="price-sale"> 
                                        {{ $production->prix }} 
                                        <span>FCFA</span>
                                    </span>
                                </p>
                            </div>
                        </div>
                        <div class="bottom-area d-flex px-3">
                            <div class="m-auto d-flex">
                                <a href="{{ route('achat.produit.show', ['slug' => $production->slug]) }}"
                                    class="add-to-cart d-flex justify-content-center align-items-center text-center">
                                    <span><i class="fa fa-eye"></i></span>
                                </a>
                                {{-- <a href="#" class="buy-now d-flex justify-content-center align-items-center mx-1">
                                    <span><i class="fa fa-cart-plus"></i></span>
                                </a>
                                <a href="#" class="heart d-flex justify-content-center align-items-center ">
                                    <span><i class="fa fa-heart"></i></span>
                                </a> --}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @empty
            <div class="alert alert-info alert-dismissible" role="alert">
                    <strong> Oups !!! </strong> Tous nos produits agricoles sont finies !
                </div>
            @endforelse
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center mb-3 pb-3"  id="elevage">
            <div class="col-md-12 heading-section text-center ftco-animate">
                {{-- <span class="subheading">Produ Products</span> --}}
                <h2 class="mb-4">Nos Produits d'élévage</h2>
                <p></p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @forelse ($productionsE as $production)
            <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="product">
                        <a href="#" class="img-prod"><img class="img-fluid"
                                src=" 
                                {{$production->url_image != null ? asset('storage/'.$production->url_image) : asset($production->produit->url_image) }} "
                                alt="Colorlib Template">
                            <span class="status">Frais</span>
                            <div class="overlay"></div>
                        </a>
                        <div class="text py-3 pb-4 px-3 text-center">
                            <h3><a href="#"> {{ $production->produit->designation }} </a></h3>
                            <div class="d-flex">
                                <div class="pricing">
                                    <p class="price">
                                        {{-- <span class="mr-2 price-dc">$120.00</span> --}}
                                        <span class="price-sale"> 
                                            {{ $production->prix }} 
                                            <span>FCFA</span>
                                        </span>
                                    </p>
                                </div>
                            </div>
                            <div class="bottom-area d-flex px-3">
                                <div class="m-auto d-flex">
                                    <a href="{{ route('achat.produit.show', ['slug' => $production->slug]) }}"
                                        class="add-to-cart d-flex justify-content-center align-items-center text-center">
                                        <span><i class="fa fa-eye"></i></span>
                                    </a>
                                    {{-- <a href="#" class="buy-now d-flex justify-content-center align-items-center mx-1">
                                        <span><i class="fa fa-cart-plus"></i></span>
                                    </a>
                                    <a href="#" class="heart d-flex justify-content-center align-items-center ">
                                        <span><i class="fa fa-heart"></i></span>
                                    </a> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
            <div class="container text-center">
                    <div class="alert alert-info alert-dismissible" role="alert">
                            <strong> Oups !!! </strong> Tous nos produits d'élevage sont finis !
                    </div>
            </div>
            @endforelse
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center mb-3 pb-3" id="halieutique">
            <div class="col-md-12 heading-section text-center ftco-animate">
                {{-- <span class="subheading">Produ Products</span> --}}
                <h2 class="mb-4">Nos Produits halieutique</h2>
                <p></p>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            @forelse ($productionsH as $production)
            <div class="col-md-6 col-lg-3 ftco-animate">
                    <div class="product">
                        <a href="#" class="img-prod"><img class="img-fluid"
                                src=" 
                                {{$production->url_image != null ? asset('storage/'.$production->url_image) : asset($production->produit->url_image) }} "
                                alt="Colorlib Template">
                            <span class="status">Frais</span>
                            <div class="overlay"></div>
                        </a>
                        <div class="text py-3 pb-4 px-3 text-center">
                            <h3><a href="#"> {{ $production->produit->designation }} </a></h3>
                            <div class="d-flex">
                                <div class="pricing">
                                    <p class="price">
                                        {{-- <span class="mr-2 price-dc">$120.00</span> --}}
                                        <span class="price-sale"> 
                                            {{ $production->prix }} 
                                            <span>FCFA</span>
                                        </span>
                                    </p>
                                </div>
                            </div>
                            <div class="bottom-area d-flex px-3">
                                <div class="m-auto d-flex">
                                    <a href="{{ route('achat.produit.show', ['slug' => $production->slug]) }}"
                                        class="add-to-cart d-flex justify-content-center align-items-center text-center">
                                        <span><i class="fa fa-eye"></i></span>
                                    </a>
                                    {{-- <a href="#" class="buy-now d-flex justify-content-center align-items-center mx-1">
                                        <span><i class="fa fa-cart-plus"></i></span>
                                    </a>
                                    <a href="#" class="heart d-flex justify-content-center align-items-center ">
                                        <span><i class="fa fa-heart"></i></span>
                                    </a> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @empty
            <div class="alert alert-info alert-dismissible" role="alert">
                    <strong> Oups !!! </strong> Tous nos produits halieutiques sont finis !
                </div>
            @endforelse
        </div>
    </div>
</section>