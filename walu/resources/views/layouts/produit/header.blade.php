<section id="home-section" class="hero">
        <div class="home-slider owl-carousel">
            <div class="slider-item" style="background-image: url({{ asset($agricole->url_image)}});">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
    
                    <div class="col-md-12 ftco-animate text-center" style="background: rgba(0, 0, 0, 0.3)">
                            <h1 class="mb-2">Nos produits agricoles sont 100% frais</h1>
                            <h2 class="subheading mb-4">Vous trouverez ici des fruits, des legumes, des céreals</h2>
                            <p><a href="#agricole" class="btn btn-primary">Voir plus</a></p>
                        </div>
    
                    </div>
                </div>
            </div>
    
            <div class="slider-item" style="background-image: url({{asset($halieutique->url_image)}});">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
    
                        <div class="col-sm-12 ftco-animate text-center" style="background: rgba(0, 0, 0, 0.3)">
                            <h1 class="mb-2">Nos produits halieutiques sont authentiques et viennent tous fraichement des
                                eaux</h1>
                            <h2 class="subheading mb-4">Vous trouverez du poisson, des crustacés, et bien d'autres</h2>
                            <p><a href="#halieutique" class="btn btn-primary">Voir plus</a></p>
                        </div>
    
                    </div>
                </div>
            </div>

            <div class="slider-item" style="background-image: url({{asset($elevage->url_image)}});">
                <div class="overlay"></div>
                <div class="container">
                    <div class="row slider-text justify-content-center align-items-center" data-scrollax-parent="true">
    
                        <div class="col-md-12 ftco-animate text-center" style="background: rgba(0, 0, 0, 0.3)">
                            <h1 class="mb-2">Nos produits d'élevage sont 100% bio</h1>
                            <h2 class="subheading mb-4">Vous trouverez ici des boeuf, des moutons, des volailles et bien d'autre</h2>
                            <p><a href="#elevage" class="btn btn-primary">Voir plus</a></p>
                        </div>
    
                    </div>
                </div>
            </div>
            

        </div>
    </section>