<section class="ftco-section ftco-category ftco-no-pt">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-6 order-md-last align-items-stretch d-flex">
                        <div class="category-wrap-2 ftco-animate img align-self-stretch d-flex"
                            style="background-image: url({{asset($catProduit->url_image)}});">
                            <div class="text text-center">
                                <h2>Des {{ $catProduit->slug }} </h2>
                            <p style="color:#fff; background: rgba(0, 0, 0, 0.3)">Les légumes sont issus directement issus de la campagne</p>
                                <p><a href="#" class="btn btn-primary">Acheter maintenant</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        @foreach ($catProduitsG as $cat)

                        <div class="category-wrap ftco-animate img mb-4 d-flex align-items-end"
                            style="background-image: url({{asset($cat->url_image)}});">
                            <div class="text px-3 py-1">
                                <h2 class="mb-0"><a href="#">{{ $cat->nom }} </a></h2>
                            </div>
                        </div>
                        @endforeach
                </div>
            </div>
        </div>

        <div class="col-md-4">
            @foreach ($catProduitsD as $cat)
            <div class="category-wrap ftco-animate img mb-4 d-flex align-items-end"
            style="background-image: url({{asset($cat->url_image)}});">
            <div class="text px-3 py-1">
                <h2 class="mb-0"><a href="#">Des  {{ $cat->nom }} </a></h2>
            </div>
        </div>
            @endforeach
        </div>
    </div>
    </div>
</section>