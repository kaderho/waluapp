<section class="ftco-section">
    <div class="container">
        <div class="row no-gutters ftco-services">
            <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services mb-md-0 mb-4">
                    <div class="icon bg-color-1 active d-flex justify-content-center align-items-center mb-2">
                        <span class="fas fa-truck"></span>
                    </div>
                    <div class="media-body">
                        <h3 class="heading">Livraison sûre</h3>
                        <span>Commande livrée où que vous soyez</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services mb-md-0 mb-4">
                    <div class="icon bg-color-2 d-flex justify-content-center align-items-center mb-2">
                        <span class="fas fa-temperature-high"></span>
                    </div>
                    <div class="media-body">
                        <h3 class="heading">Toujours frais</h3>
                        <span>Produits biens emballés</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services mb-md-0 mb-4">
                    <div class="icon bg-color-3 d-flex justify-content-center align-items-center mb-2">
                        <span class="fas fa-chart-line"></span>
                    </div>
                    <div class="media-body">
                        <h3 class="heading">Qualité supérieure</h3>
                        <span>Produits de qualité</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-center d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services mb-md-0 mb-4">
                    <div class="icon bg-color-4 d-flex justify-content-center align-items-center mb-2">
                        <span class="fas fa-handshake"></span>
                    </div>
                    <div class="media-body">
                        <h3 class="heading">Soutien</h3>
                        <span>Assistance 24/7</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>