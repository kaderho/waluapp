<div class="modal-body">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                   
                    <input type="text" class="form-control" name="libelle" id="nom"
                        placeholder="Libellé" value="{{ isset($ressource)?$ressource->libelle:old('libelle') }}">

                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    <input type="number" class="form-control" name="quantite" id="nom" value="{{ isset($ressource)?$ressource->quantite:old('quantite') }}"
                        placeholder="Quantité">
                </div>   
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    <input type="number" class="form-control" name="montant" id="nom"
                        placeholder="Montant" value="{{ isset($ressource) ? $ressource->montant : old('montant') }}">
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-12">
                <div class="form-group">
                    <input type="file" class="form-control" name="image" id="date_fin">
                </div>
            </div>
        </div>

    </div>
</div>