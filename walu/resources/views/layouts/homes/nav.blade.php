<header class="header header_style_01">
    <nav class="megamenu navbar  navbar-default navbar-expand">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="{{ url('/') }}" class="" style="font-size: 20px; font-weight: 900; color:#fd6802; letter-spacing: 1px"> {{ config('app.name') }} </a>
                {{-- <a class="navbar-brand" href="index.html"><img src="{{ asset('dist/images/logos/logo-walu1.png') }}" alt="image"></a> --}}
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li><a class=" {{ set_active('accueil') }} " href="{{ route('home') }}">Acueil</a></li>
                    <li><a class=" {{ set_active('a-propos-de-nous') }} " href="{{ route('apropos') }}">à propos de nous</a></li>
                    <li><a class=" {{ set_active('produits') }} " href="{{ route('achat') }}">Nos produits</a></li>
                    <li><a class=" {{ set_active('contact') }} " href="">Contact</a></li>
                    {{-- <li><a class=" {{ set_active('contact') }} " href="{{ route('contact') }}">Contact</a></li> --}}
                   
                    @if (auth()->check())
                        <li><a class=" {{ set_active('connexion') }} " href="{{ route('user') }}"><i class="fa fa-user"></i> Mon compte</a></li>                        
                    @else
                        <li><a class=" {{ set_active('connexion') }} " href="{{ route('connexion') }}">Connexion</a></li>
                        <li><a class=" {{ set_active('inscription') }} " href="{{ route('inscription') }}">Inscription</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
</header>