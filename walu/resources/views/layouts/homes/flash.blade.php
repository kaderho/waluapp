@if (session('blue'))
<div class="container">
    <div class="row">
        <div class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <strong> {!! session('title') !!} </strong> {!! session('blue') !!}
        </div>
    </div>
</div>
@endif

@if (session('red'))
<div class="container">
    <div class="row">
        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <strong> {!! session('title') !!} </strong> {!!  session('red') !!}
        </div>
    </div>
</div>
@endif

@if (session('orange'))
<div class="container">
    <div class="row">
        <div class="alert alert-warning alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <strong> {!! session('title') !!} </strong> {!! session('orange') !!}
        </div>
    </div>
</div>
@endif

@if (session('green'))
<div class="container">
    <div class="row">
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                    aria-hidden="true">&times;</span></button>
            <strong> {!! session('title') !!} </strong> {!! session('green') !!}
        </div>
    </div>
</div>
@endif

@if (session('violet'))
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <strong> {!! session('title') !!} </strong> {!! session('violet') !!}
</div>
@endif

@if (session('teal'))
<div class="alert alert-success alert-dismissible" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
            aria-hidden="true">&times;</span></button>
    <strong> {!! session('title') !!} </strong> {!! session('teal') !!}
</div>
@endif