<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="@yield('meta-description')">
    <meta name="author" content="">

    <title>@yield('meta-title')</title>

    <!-- Custom fonts for this template-->
    <link href="{{asset('user/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template-->
    <link href="{{asset('user/css/sb-admin-2.min.css')}}" rel="stylesheet">

    @yield('css')

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

       @include('layouts.user.sidebar')

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

               @include('layouts.user.navigation')

               @include('layouts.homes.flash')

                <!-- Begin Page Content -->
                <div class="container-fluid">
                    @include('layouts.user.header')
                    @yield('content')
                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

          @include('layouts.user.footer')

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

   @include('layouts.user.modal-deconnecte')

    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('user/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('user/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('user/js/sb-admin-2.min.js')}}"></script>

    @yield('js')

    <!-- Page level custom scripts -->

</body>

</html>