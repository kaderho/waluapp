<!DOCTYPE html>
<html lang="en">

<head>
    <title></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Lora:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Amatic+SC:400,700&display=swap" rel="stylesheet">

    <link href="{{asset('user/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="{{asset('produits/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('produits/css/animate.css')}}">

    <link rel="stylesheet" href="{{asset('produits/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('produits/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{asset('produits/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{asset('produits/css/aos.css')}}">

    <link rel="stylesheet" href="{{asset('produits/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{asset('produits/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('produits/css/jquery.timepicker.css')}}">


    <link rel="stylesheet" href="{{asset('css/flaticon.css')}}">
    <link rel="stylesheet" href="{{asset('produits/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{asset('produits/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('produits/css/menu.css')}}">

    @yield('css')
</head>

<body class="goto-here">
    @include('layouts.produit.nav')
    <!-- END nav -->

    @yield('content')
    @include('layouts.user.footer')

    <script src="{{asset('produits/js/jquery.min.js')}}"></script>
    <script src="{{asset('produits/js/axios.min.js')}}"></script>
    <script src="{{asset('produits/js/jquery-migrate-3.0.1.min.js')}}"></script>
    <script src="{{asset('produits/js/popper.min.js')}}"></script>
    <script src="{{asset('produits/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('produits/js/jquery.easing.1.3.js')}}"></script>
    <script src="{{asset('produits/js/jquery.waypoints.min.js')}}"></script>
    <script src="{{asset('produits/js/jquery.stellar.min.js')}}"></script>
    <script src="{{asset('produits/js/owl.carousel.min.js')}}"></script>
    <script src="{{asset('produits/js/jquery.magnific-popup.min.js')}}"></script>
    <script src="{{asset('produits/js/aos.js')}}"></script>
    <script src="{{asset('produits/js/jquery.animateNumber.min.js')}}"></script>
    <script src="{{asset('produits/js/bootstrap-datepicker.js')}}"></script>
    <script src="{{asset('produits/js/scrollax.min.js')}}"></script>
    <script src="{{asset('produits/js/main.js')}}"></script>
    @yield('js')

</body>

</html>