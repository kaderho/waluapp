<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Site Metas -->
    <title>@yield('meta-title')</title>
    <meta name="keywords" content="">
    <meta name="description" content="@yield('meta-description')">
    <meta name="author" content="">

    <!-- Site Icons -->
    {{-- <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" /> --}}
    {{-- <link rel="apple-touch-icon" href="images/apple-touch-icon.png"> --}}
    <!-- Bootstrap CSS -->
    {{-- <link rel="stylesheet" href="{{asset('dist/css/bootstrap4/bootstrap.min.css')}}"> --}}

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('dist/css/bootstrap.min.css')}}">
    <!-- Site CSS -->
    <link rel="stylesheet" href="{{asset('dist/style.css')}}">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="{{asset('dist/css/responsive.css')}}">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{asset('dist/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{asset('dist/css/custom.css')}}">

    @yield('css')

    <!-- Modernizer for Portfolio -->
    <script src="{{asset('dist/js/modernizer.js')}}"></script>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    {{-- loader --}}
    {{-- @include('layouts.app.loader') --}}

    {{-- navigation --}}
    @include('layouts.homes.nav')

    {{-- banner --}}
    {{-- @include('layouts.app.banner') --}}

    {{-- message flash --}}
    <div style="margin-top:10px;">
        @include('layouts.homes.flash')
    </div>

    {{-- contenu --}}
    @yield('content')

    {{-- footer --}}
    @include('layouts.user.footer')

    <!-- ALL JS FILES -->
    <script src="{{asset('dist/js/all.js')}}"></script>
    <script src="{{asset('dist/js/owl.carousel.js')}}"></script>
    <!-- ALL PLUGINS -->
    <script src="{{asset('dist/js/custom.js')}}"></script>
    <script src="{{asset('dist/js/portfolio.js')}}"></script>
    <script src="{{asset('dist/js/hoverdir.js')}}"></script>

    @yield('js')

</body>

</html>