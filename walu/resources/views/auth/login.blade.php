@extends('templates.home')

{{-- description --}}
@section('meta-description', $description = "")

{{-- titre de la page --}}
@section('meta-title', $title = "Se connecter")

@section('css')
<style type="text/css">
    body {
        color:;
        background: #fff;
        font-family: 'Open Sans', sans-serif;
    }

    .form-control:focus {
        border-color: #99c432;
    }

    .login-form {
        color: ;
        border-radius: 10px;
        margin-bottom: 15px;
        background: #fff;
        box-shadow: 2px 2px 2px 2px rgba(0, 0, 0, 0.3);
        padding: 30px;
    }

    .login-form h2 {
        font-size: 24px;
        color: #454959;
        margin: 45px 0 25px;
        font-family: 'Francois One', sans-serif;
    }

    .login-form .avatar {
        position: absolute;
        margin: 0 auto;
        left: 0;
        right: 0;
        top: -50px;
        width: 95px;
        height: 95px;
        border-radius: 50%;
        z-index: 9;
        background: #fd6802;
        padding: 15px;
        box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.1);
    }

    .login-form .avatar img {
        width: 100%;
    }

    .social-btn {
        padding-bottom: 15px;
    }

    /* .social-btn .btn {
        margin-bottom: 10px;
        font-size: 14px;
        text-align: left;
    } */

    .social-btn .btn:hover {
        opacity: 0.8;
        text-decoration: none;
    }

    .social-btn .btn-primary {
        background: #507cc0;
    }

    .social-btn .btn-info {
        background: #64ccf1;
    }

    .social-btn .btn-danger {
        background: #df4930;
    }

    /* .social-btn .btn i {
        float: left;
        margin: 1px 10px 0 5px;
        min-width: 20px;
        font-size: 18px;
    } */

    .or-seperator {
        height: 0;
        margin: 0 auto 20px;
        text-align: center;
        border-top: 1px solid #e0e0e0;
        width: 30%;
    }

    .or-seperator i {
        padding: 0 10px;
        font-size: 15px;
        text-align: center;
        background: #fff;
        display: inline-block;
        position: relative;
        top: -13px;
        z-index: 1;
    }

    .login-form a {
        color:;
        text-decoration: underline;
    }

    .login-form form a {
        color: ;
        text-decoration: none;
    }

    .login-form a:hover,
    .login-form form a:hover {
        text-decoration: none;
    }

    .login-form form a:hover {
        text-decoration: underline;
    }

    .btn-fb{
    background: #3b5998;
    color: #fff;
    }

    .btn-fb:hover{
    background: #fff;
    color: #3b5998;
    border:solid #3b5998 1px;
    }

    .btn-gl{
    background: #db4437;
    color: #fff;
    }
    
    .btn-gl:hover{
    background: #fff;
    color: #db4437;
    border:solid #db4437 1px;
    }
</style>
@endsection

@section('content')
<div class="container" style="margin-top:75px; margin-bottom:50px;">
    <div class="row">
        <div class="">
            <div class="login-form col-md-6 col-md-offset-3">
                <form action="" method="POST">
                    @csrf
                   
                    <h2 class="text-center"> {{ $title }} </h2>
                    <div class="social-btn text-center">
                        <a href="#" class="btn btn-fb btn-block btn-lg"><i class="fa fa-facebook"></i> Se connecter avec
                            <b>Facebook</b></a>
                        <a href="#" class="btn btn-gl btn-block btn-lg"><i class="fa fa-google"></i> Se connecter avec
                            <b>Google</b></a>
                    </div>
                    <div class="or-seperator"><i>ou</i></div>
                    <div class="form-group">
                        <input type="text" class="form-control input-lg" name="email" placeholder="Adresse Email"
                            required="required">
                    </div>
                    <input type="hidden" name="redirect" value=" {{ $_SERVER['HTTP_REFERER'] ?? '/' }} ">
                    <div class="form-group">
                        <input type="password" class="form-control input-lg" name="password" placeholder="Mot de Passe"
                            required="required">
                    </div>
                    <div class="form-group" style="margin-left:10px">
                        <input type="checkbox" class="" id="remember" name="remember">
                        <label for="remember">Se souvenir de moi</label>
                    </div>
                    <p class="text-center small"><a href="#">Mot de passe oublié ?</a></p>
                    <div class="form-group">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                            <button type="submit" value="SEND" id="submit"
                                class="btn btn-light btn-radius btn-brd grd1 btn-block"><i class="fa fa-lock"></i> {{ $title }} </button>
                        </div>
                    </div>
                </form>
                <p class="text-center small">Pas encore de compte ? <a href="{{ route('inscription') }}">Inscrivez-vous ici !</a></p>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
{{-- cotenu js --}}
@endsection