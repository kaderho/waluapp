@extends('templates.home')

{{-- description --}}
@section('meta-description', $description = "")

{{-- titre de la page --}}
@section('meta-title', $title = "Inscription")

@section('css')
@endsection

@section('content')
<div class="container">
    <div class="row text-center">
        <h2 title=""> {{ $title }} </h2>
    </div>
         <form id="loginform" class="" role="form" method="POST" action="">
                    @csrf
                   
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 form-group">
                            <div style="" class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input id="login-name" type="text" class="form-control input-lg" name="name"
                                    value="{{old('name')}}" placeholder="Votre Prénom(s) et Nom">
                            </div>
                    {!! $errors->first('name', '<span class="text-danger"></span>') !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 col-md-offset-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                <input id="login-email" type="text" class="form-control input-lg" name="email"
                                    value="{{old('email')}}" placeholder="Votre Email">
                                {!! $errors->first('email', '<span class="text-danger">:message</span>') !!}
                                </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 col-md-offset-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-map-marker"></i></span>
                                <input id="login-adresse" type="text" class="form-control input-lg" name="adresse"
                                    value="{{old('adresse')}}" placeholder="Votre Adresse">
                                    {!! $errors->first('adresse', '<span class="text-danger">:message</span>') !!}
                                </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 form-group col-md-offset-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                                <input id="login-telephone" type="text" class="form-control input-lg" name="telephone"
                                    value="{{old('telephone')}}" placeholder="Votre Numéro de Téléphone">
                                    {!! $errors->first('telephone', '<span class="text-danger">:message</span>') !!}
                                </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 form-group col-md-offset-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input id="login-password" type="password" class="form-control input-lg" name="password"
                                    value="{{old('password')}}" placeholder="Nouveau Mot de Passe">
                                    {!! $errors->first('password', '<span class="text-danger">:message</span>') !!}
                                </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 form-group col-md-offset-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                                <input id="login-confirm_password" type="password" class="form-control input-lg"
                                    name="confirm_password" value="{{old('confirm_password')}}"
                                    placeholder="Confirmez votre Mot de Passe">
                                    {!! $errors->first('confirm_password', '<span class="text-danger">:message</span>') !!}
                                </div>
                        </div>
                    </div>
                    <div class="row">   
                        <div class="form-group col-md-6 col-md-offset-3">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-question"></i></span>
                                <select name="role_id" id="role_id" class="form-control input-lg">
                                    <option value="">Vous êtes ?</option>
                                    <option value="3">Achêteur</option>
                                    <option value="2">Agriculteur</option>
                                    <option value="4">Eleveur</option>
                                    <option value="5">Investisseur</option>
                                    <option value="7">Pêcheur</option>
                                </select>
                                {!! $errors->first('role_id', '<span class="text-danger">:message</span>') !!}
                        </div>
                    </div> 
                </div>
                    
                    <div class="row">
                        <!-- Button -->
                        <div class="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                            <button type="submit" value="" id="valider"
                                class="btn btn-light btn-radius btn-brd  btn-block">
                                Valider
                            </button>
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group col-md-6 control  col-md-offset-3 text-center">
                            <hr>
                                vous êtes membre ?
                                <a href="#">
                                    Connectez-vous ici
                                </a>
                        </div>
                    </div>
                </div>

                </form>
</div>
    

@endsection

@section('js')
{{-- <script>
    $('#role_id').change(function() {
        if (this.value != 'acheteur') {
            $('#valider').html('Suivant');
        }
        else{
            $('#valider').html('Valider');
        }

       });
</script>  --}}
@endsection