var data = [
    "Amsterdam",
    "Atlanta",
    "Berlin",
    "Boston",
    "Brussels",
    "Copenhagen",
    "Denver",
    "Dresden",
    "Eindhoven",
    "Frankfurt",
    "Grenoble",
    "Hanover",
    "Hamburg",
    "Innsbruck",
    "Jerusalem",
    "Kaiserslautern",
    "Kiyv",
    "Lisbon",
    "London",
    "Moscow",
    "Moss",
    "Moselle",
    "New York",
    "New Hampton",
    "Newton",
    "Orlando",
    "Paris",
    "Prague",
    "Quedlinburg",
    "Riga",
    "South Hampton",
    "Saint Louis",
    "San Diego",
    "San Andreas",
    "Saint Petersburg",
    "Tampa",
    "Tampere",
    "Tomsk",
    "Udestedt",
    "Vogelsberg",
    "Washington",
    "Xian",
    "Yadkinville",
    "Zanesville"
];

var $searchForm = $(".search-form"),
    $searchInput = $(".search-input"),
    $resultsContainer = $(".search-result"),
    $btnTop = 38,
    $btnBottom = 40,
    $btnEnter = 13;

var activeResultIndex = 0;
var $resultItem = null;

function fadeResult(i) {
    $($resultItem[i]).removeClass("result-item__hover");
}

function highlightResult(i) {
    $($resultItem[i]).addClass("result-item__hover");
}

function keyboardSelection(e) {
    if (e.keyCode === $btnBottom) {
        fadeResult(activeResultIndex);
        if (activeResultIndex === $resultItem.length) {
            activeResultIndex = 0;
            highlightResult(activeResultIndex);
        } else {
            activeResultIndex++;
            highlightResult(activeResultIndex);
        }
    } else if (e.keyCode === $btnTop) {
        fadeResult(activeResultIndex);
        if (activeResultIndex === 0) {
            activeResultIndex = $resultItem.length;
            highlightResult(activeResultIndex);
        } else {
            activeResultIndex--;
            highlightResult(activeResultIndex);
        }
    } else if (e.keyCode === $btnEnter) {
        selectValue();
    }
}

function mouseOver(e) {
    if ($(e.target).hasClass("result-item")) {
        fadeResult(activeResultIndex);
        activeResultIndex = $(e.target).index();
        highlightResult($(e.target).index());
    }
}

function selectValue() {
    $searchInput.val($($resultItem[activeResultIndex]).text());
    hideResults();
}

function displayResults(value) {
    $resultsContainer.append("<p class='result-item'>" + value + "</p>");
    $resultItem = $(".result-item");
    activeResultIndex = 0;
    $($resultItem[activeResultIndex]).addClass("result-item__hover");
    if ($resultsContainer.outerHeight() > 210) {
        $resultsContainer.addClass("results-scroll");
    } else {
        $resultsContainer.removeClass("results-scroll");
    }
}

function hideResults() {
    $resultsContainer.find("p").remove();
    $resultsContainer.fadeOut();
    $resultItem = null;
    activeResultIndex = 0;
    console.log('hide')
}

function searchData(e) {
    var inputValue = $searchInput.val();
    if (inputValue.length) {
        getResults(e, inputValue);
    } else {
        hideResults();
    }
}

function searchLetter(e) {
    let vt = $(this).val().toLowerCase();

    if (vt.length) {
        $resultsContainer.fadeIn();
        if (e.keyCode === $btnBottom || e.keyCode === $btnTop || e.keyCode === $btnEnter) {
            keyboardSelection(e);
        } else {
            $resultsContainer.find("p").remove();
            for (let i = 0; i < data.length; i++) {
                let dataT = data[i].toLowerCase().indexOf(vt);
                let dataMax = dataT + vt.length;
                if (dataT >= 0) {
                    $resultItem = $(".result-item");
                    $resultsContainer.append("<p class='result-item'>" + arrayString(data[i], dataT, dataMax) + "</p>");
                }
            }
        }
    } else {
        hideResults();
    }
}

function arrayString(str, fMin, fMax) {
    let str2 = str.substring(fMin, fMax)
    let strArr = str.split('');
    let ost = strArr.slice(fMin, fMax)
    let ost2 = ost.map(function (curent) {
        return '<span>' + curent + '</span>'
    })

    for (let i = 0; i < ost2.length; i++) {
        strArr.splice(i + fMin, 1, ost2[i])
    }

    return strArr.join('')
}

$searchInput.keyup(searchLetter);
$resultsContainer.on("mouseover", mouseOver);
$resultsContainer.on("click", $resultItem, selectValue);

$(document).on("keypress", "form", function (e) {
    return e.keyCode != $btnEnter;
});