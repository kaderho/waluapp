<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('quantite');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('produit_id');
            $table->unsignedBigInteger('secteur_id');
            $table->string('slug')->unique();
            $table->string('url_image')->nullable();
            $table->string('nom_production');
            $table->enum('status', ['en cours', 'terminées'])->default('en cours');
            $table->decimal('prix',15,2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productions');
    }
}
