<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Production;
use Faker\Generator as Faker;

$factory->define(Production::class, function (Faker $faker) {
    return [
        'quantite' => $faker->numberBetween(50, 1000), 
        'prix' => $faker->numberBetween(50, 1000),
    ];
});
