<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Kader Seck',
            'adresse' => 'Ouakam',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'seckkader41@gmail.com',
            'role_id' => 1,
            'status' => 'actif'
        ]);

        User::create([
            'name' => 'Demba Soumare',
            'adresse' => 'Parcelle',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'bigs1@gmail.com',
            'role_id' => 1,
            'status' => 'actif'
        ]);


        User::create([
            'name' => 'Adbou SOW',
            'adresse' => 'Gindar',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'agri@gmail.com',
            'role_id' => 2,
            'status' => 'actif'
        ]);

        User::create([
            'name' => 'Mohamed DIOP',
            'adresse' => 'Gindar',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'agri1@gmail.com',
            'role_id' => 2,
            'status' => 'actif'
        ]);


        User::create([
            'name' => 'Khalifa SOW',
            'adresse' => 'Gindar',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'agri2@gmail.com',
            'role_id' => 2,
            'status' => 'actif'
        ]);


        User::create([
            'name' => 'Demba SOW',
            'adresse' => 'Gindar',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'agri3@gmail.com',
            'role_id' => 2,
            'status' => 'actif'
        ]);

        User::create([
            'name' => 'Adama LY',
            'adresse' => 'Parcelle',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'achet@gmail.com',
            'role_id' => 3,
            'status' => 'actif'
        ]);


        User::create([
            'name' => 'Kader LY',
            'adresse' => 'Parcelle',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'achet2@gmail.com',
            'role_id' => 3,
            'status' => 'actif'
        ]);

        User::create([
            'name' => 'Ousmane LY',
            'adresse' => 'Parcelle',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'achet1@gmail.com',
            'role_id' => 3,
            'status' => 'actif'
        ]);

        User::create([
            'name' => 'Ousmane MANE',
            'adresse' => 'Parcelle',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'pec@gmail.com',
            'role_id' => 7,
            'status' => 'actif'
        ]);

        User::create([
            'name' => 'Mandiallo MANE',
            'adresse' => 'Parcelle',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'pec2@gmail.com',
            'role_id' => 7,
            'status' => 'actif'
        ]);

        User::create([
            'name' => 'Biram MANE',
            'adresse' => 'Parcelle',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'pec1@gmail.com',
            'role_id' => 7,
            'status' => 'actif'
        ]);

        User::create([
            'name' => 'Khalifa NDIAYE',
            'adresse' => 'Gindar',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'elev2@gmail.com',
            'role_id' => 4,
            'status' => 'actif'
        ]);


        User::create([
            'name' => 'Demba CAMARA',
            'adresse' => 'Gindar',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'elev3@gmail.com',
            'role_id' => 4,
            'status' => 'actif'
        ]);

        User::create([
            'name' => 'Adama LAYE',
            'adresse' => 'Parcelle',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'elev@gmail.com',
            'role_id' => 4,
            'status' => 'actif'
        ]);


        User::create([
            'name' => 'Kader MBODJE',
            'adresse' => 'Parcelle',
            'telephone' => '781392844',
            'password' => 'passer',
            'email' => 'elev4@gmail.com',
            'role_id' => 4,
            'status' => 'actif'
        ]);

    }
}
