<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(CategorieProduitsTableSeeder::class);
        $this->call(ProduitsTableSeeder::class);
        $this->call(SecteursTableSeeder::class);
        $this->call(ProductionsTableSeeder::class);
    }
}
