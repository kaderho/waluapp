<?php

use App\Models\Secteur;
use Illuminate\Database\Seeder;

class SecteursTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Secteur::create(['nom_secteur' => 'Agricole', 'url_image' => 'produits/images/bg_2.jpg']);
        Secteur::create(['nom_secteur' => 'Elevage', 'url_image' => 'produits/images/bg_4.jpg']);
        Secteur::create(['nom_secteur' => 'Halieutique', 'url_image' => 'produits/images/bg_5.jpg']);
    }
}
