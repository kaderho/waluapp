<?php

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create(['nomRole' => 'Administrateur', 'slug' => 'administrateur']);
        Role::create(['nomRole' => 'Agriculteur', 'slug' => 'agriculteur']);
        Role::create(['nomRole' => 'Acheteur', 'slug' => 'acheteur']);
        Role::create(['nomRole' => 'Eleveur', 'slug' => 'eleveur']);
        Role::create(['nomRole' => 'Investisseur', 'slug' => 'investisseur']);
        Role::create(['nomRole' => 'Manager', 'slug' => 'manager']);
        Role::create(['nomRole' => 'Pêcheur', 'slug' => 'pecheur']);

    }
}
