<?php

use App\Models\Produit;
use Illuminate\Database\Seeder;

class ProduitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Produit::create([
            'designation'=>'Tomate', 
            'categorie_produit_id'=>'2', 
            'url_image'=>'produits/images/product-3.jpg', 
            'secteur_id'=>'1']);

            Produit::create([
                'designation'=>'Oignon', 
                'categorie_produit_id'=>'2', 
                'url_image'=>'produits/images/product-9.jpg', 
                'secteur_id'=>'1']);

                Produit::create([
                    'designation'=>'Piment', 
                    'categorie_produit_id'=>'2', 
                    'url_image'=>'produits/images/product-3.jpg', 
                    'secteur_id'=>'1']);

                    Produit::create([
                        'designation'=>'Carotte', 
                        'categorie_produit_id'=>'2', 
                        'url_image'=>'produits/images/product-7.jpg', 
                        'secteur_id'=>'1']);

                        Produit::create([
                            'designation'=>'Pomme', 
                            'categorie_produit_id'=>'1', 
                            'url_image'=>'produits/images/product-10.jpg', 
                            'secteur_id'=>'1']);

                            Produit::create([
                                'designation'=>'Mangue', 
                                'categorie_produit_id'=>'1', 
                                'url_image'=>'produits/images/product-15.jpg', 
                                'secteur_id'=>'1']);

                                Produit::create([
                                    'designation'=>'Banane', 
                                    'categorie_produit_id'=>'1', 
                                    'url_image'=>'produits/images/product-14.jpg', 
                                    'secteur_id'=>'1']);

                                    Produit::create([
                                        'designation'=>'Orange', 
                                        'categorie_produit_id'=>'1', 
                                        'url_image'=>'produits/images/product-13.jpg', 
                                        'secteur_id'=>'1']);

                                        Produit::create([
                                            'designation'=>'Mouton', 
                                            'categorie_produit_id'=>'6', 
                                            'url_image'=>'produits/images/product-16.jpg', 
                                            'secteur_id'=>'2']);

                                            Produit::create([
                                                'designation'=>'Boeuf', 
                                                'categorie_produit_id'=>'6', 
                                                'url_image'=>'produits/images/product-17.jpg', 
                                                'secteur_id'=>'2']);

                                                Produit::create([
                                                    'designation' => 'Chèvre',
                                                    'categorie_produit_id' => '6',
                                                    'url_image' => 'produits/images/product-17.jpg',
                                                    'secteur_id' => '2'
                                                ]);

                                                Produit::create([
                                                    'designation'=>'Ton', 
                                                    'categorie_produit_id'=>'5', 
                                                    'url_image'=>'produits/images/product-18.jpg', 
                                                    'secteur_id'=>'3']);

                                                    Produit::create([
                                                        'designation'=>'Thiof', 
                                                        'categorie_produit_id'=>'5', 
                                                        'url_image'=>'produits/images/product-16.jpg', 
                                                        'secteur_id'=>'3']);

                                                        Produit::create([
                                                            'designation'=>'Dorade', 
                                                            'categorie_produit_id'=>'5', 
                                                            'url_image'=>'produits/images/product-19.jpg', 
                                                            'secteur_id'=>'3']);
                                                            Produit::create([

                                                                'designation'=>'Yaboye', 
                                                                'categorie_produit_id'=>'5', 
                                                                'url_image'=>'produits/images/product-20.jpg', 
                                                                'secteur_id'=>'3']);

        Produit::create([
            'designation' => 'Poulet',
            'categorie_produit_id' => '7',
            'url_image' => 'produits/images/product-17.jpg',
            'secteur_id' => '2'
        ]);

        Produit::create([
            'designation' => 'Pintade',
            'categorie_produit_id' => '7',
            'url_image' => 'produits/images/product-17.jpg',
            'secteur_id' => '2'
        ]);

        Produit::create([
            'designation' => 'Oeuf de poulet',
            'categorie_produit_id' => '7',
            'url_image' => 'produits/images/product-17.jpg',
            'secteur_id' => '2'
        ]);
    }
}
