<?php

use App\Models\CategorieProduit;
use Illuminate\Database\Seeder;

class CategorieProduitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategorieProduit::create(['nom' => 'Fruits', 'url_image' => 'produits/images/category-3.jpg']);
        CategorieProduit::create(['nom' => 'Légumes', 'url_image' => 'produits/images/category.jpg']);
        CategorieProduit::create(['nom' => 'graines', 'url_image' => 'produits/images/category-4.jpg']);
        CategorieProduit::create(['nom' => 'Crustacés', 'url_image' => 'produits/images/category-8.jpg']);
        CategorieProduit::create(['nom' => 'Poissons', 'url_image' => 'produits/images/category-7.jpg']);
        CategorieProduit::create(['nom' => 'Betail', 'url_image' => 'produits/images/category-10.jpg']);
        CategorieProduit::create(['nom' => 'Volaille', 'url_image' => 'produits/images/category-11.jpg']);
    }
}
