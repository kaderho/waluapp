<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['prefix' => '/', 'namespace' => 'Home'], function () {
    
    Route::get('', 'HomeController@home')->name('home');
    Route::get('projets', 'HomeController@projet')->name('investir');
    Route::get('contact', 'HomeController@contact')->name('contact');
    Route::get('a-propos-de-nous', 'HomeController@apropos')->name('apropos');
    Route::get('users', 'HomeController@user')->name('user');

    Route::group(['prefix' => 'acheter'], function () {
        
        Route::get('', 'AcheterController@index')->name('achat');
        Route::get('{produit}/details', 'AcheterController@show')->name('achat.produit.show');
             
    });

    Route::group(['prefix' => 'panier'], function () {

        Route::post('/ajouter', 'PanierController@store')->name('panier.store');
        
        Route::delete('{rowId}/supprimer', 'PanierController@destroy')->name('panier.destroy');
        Route::patch('{rowId}/mise-a-jour', 'PanierController@update')->name('panier.update');      
        Route::get('/vider', 'PanierController@empty')->name('panier.vider');        
        Route::get('', 'PanierController@index')->name('panier');
    });
});

Route::group(['namespace' => 'Auth'], function () {
    
    Route::get('connexion', 'ConnexionController@create')->name('connexion');
    Route::post('connexion', 'ConnexionController@store');
    Route::get('logout', 'ConnexionController@destroy')->name('logout');
    Route::get('validation/{user}/{token}', 'ConnexionController@validation')->name('validation');

    Route::get('inscription', 'InscriptionController@create')->name('inscription');
    Route::post('inscription', 'InscriptionController@store');

});

Route::group(['prefix' => 'admin', 'namespace' => 'Admin'], function () {
    
    Route::get('', 'AdminController@index')->name('admin.index');

    Route::get('producteurs/{user}', 'AdminController@user')->name('user.list');
    Route::get('producteurs/{user}/details', 'AdminController@user')->name('user.show.a');

    Route::get('projet/{projet}', 'AdminController@projet')->name('projet.list');
    Route::get('projet/{projet}/details', 'AdminController@projet')->name('projet.show.a');


    Route::group(['prefix' => 'produits'], function () {

        Route::get('', 'ProduitController@index')->name('produit.index');
        Route::get('{produit}/detail', 'ProduitController@index')->name('produit.show');
        Route::post('nouveau', 'ProduitController@store')->name('produit.store');

        Route::delete('delete/{produit}', 'ProduitController@destroy')->name('produit.destroy');

        Route::put('{produit}/update', 'ProduitController@update')->name('produit.update');

        Route::post('categorie-produit', 'ProduitController@cat_store')->name('categorieProduit.store');
        
    });
});

Route::group(['prefix' => 'producteur', 'namespace' => 'Producteurs'], function () {
    
    Route::get('', 'ProducteurController@index')->name('producteur.index');

    Route::group(['prefix' => 'activites'], function () {

        Route::get('ressources', 'RessourceController@index')->name('ressource');

        Route::get('ressources/nouvelle', 'RessourceController@create')->name('ressource.create');
        Route::post('/ressources/nouvelle', 'RessourceController@store');
        
        Route::get('/ressources/{slug}/editer', 'RessourceController@edit')->name('ressource.edit');
        Route::put('/ressources/{slug}/editer', 'RessourceController@update');
        
        Route::delete('/ressources/{slug}/supprimer', 'RessourceController@destroy')->name('ressource.delete');

    });

    Route::group(['prefix' => 'productions'], function () {

        Route::get('nouveau', 'ProductionController@create')->name('production.create');
        Route::post('nouveau', 'ProductionController@store');
        
        Route::get('{status}', 'ProductionController@index')->name('production');

    });
});

Route::group(['prefix' => 'investisseur', 'namespace' => 'Investisseurs'], function () {
    
    Route::get('', 'InvestisseurController@index')->name('investisseur.index');
});

Route::group(['prefix' => 'users'], function () {

    Route::get('{user}/profile', 'UserController@profileIndex')->name('profile.index');
    Route::put('{user}/profile', 'UserController@profileUpdate')->name('profile.update');
});

Route::group(['prefix' => 'acheteur', 'namespace' => 'Acheteurs'], function () {
    
    Route::get('', 'AcheteurController@index')->name('acheteur.index');
});