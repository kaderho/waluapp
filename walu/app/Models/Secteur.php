<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Secteur extends Model
{
    protected $fillable = ['nom_secteur', 'url_image', 'slug'];

    protected static function boot(){

        parent::boot();

        self::creating(function($model){

            $model->slug = str_slug($model->nom_secteur);
        });
    }

    public function produits(){

        $this->hasMany(Produit::class);
    }

    public function productions(){

        $this->hasMany(Production::class);
    }
}
