<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commentaire extends Model
{
    protected $fillable = [
        'contenu', 'note', 'nom_categorie', 'production_id', 'user_id', 'slug'
    ];

    public function production(){
        return $this->belongsTo(Production::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }
}
