<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommandeProjet extends Model
{
    protected $fillable = [
        'quantite','commande_id', 'projet_id'
    ];

    public function commande(){
        return $this->belongsTo(Commande::class);
    }

    public function projet(){
        return $this->belongsTo(Projet::class);
    }
}
