<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommandeProduction extends Model
{
    protected $fillable = [
        'quantite','commande_id', 'production_id'
    ];

    public function commande(){
        return $this->belongsTo(Commande::class);
    }

    public function Production(){
        return $this->belongsTo(Production::class);
    }
}
