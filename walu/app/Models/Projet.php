<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Projet extends Model
{
    const EN_COURS = 'en cours';
    const TERMINE = 'terminés';
    const EN_ATTENTE = 'en attentes';

    protected $fillable = [
        'nom_projet', 'date_debut', 'produit_id', 'user_id', 
        'slug', 'description', 'date_fin', 'status',
    ];

    protected static function boot(){

        parent::boot();

        self::creating(function($model){

            $model->slug = str_slug($model->nom_projet);
            $model->user_id = auth()->user()->id;
        });
    }

    public function produit(){
        return $this->belongsTo(Produit::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function ressources(){
        return $this->hasMany(Ressource::class);
    }

    public function investissement(){
        return $this->hasOne(Investissement::class);
    }

    public function production(){
        return $this->hasOne(Production::class);
    }

    public function commandeProjets(){
        return $this->hasMany(CommandeProjet::class);
    }

    public function commandes(){
        return $this->belongsToMany(Commande::class);
    }
}
