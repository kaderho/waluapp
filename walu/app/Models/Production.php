<?php
namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Production extends Model
{
    const EN_COURS = 'en cours';
    const TERMINE = 'terminées';

    protected $fillable = [
        'quantite', 'prix', 'produit_id','secteur_id', 'slug', 'url_image', 'status', 'nom_production',
    ];

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){

            $model->slug = str_random(6);

         if (auth()->check()) {
            if (auth()->user()->role_id == 2) {
                $model->secteur_id = 1;
            }

            if (auth()->user()->role_id == 3) {
                $model->secteur_id = 2;
                # code...
            }

            if (auth()->user()->role_id == 7) {
                $model->secteur_id = 3;
                # code...
            }
         }
        });
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function secteur(){
        return $this->belongsTo(Secteur::class);
    }

    public function produit(){
        return $this->belongsTo(Produit::class);
    }

    public function commentaires(){
        return $this->hasMany(Commentaire::class);
    }

    public function commandeProductions(){
        return $this->hasMany(CommandeProduction::class);
    }

    public function commandes(){
        return $this->belongsToMany(Commande::class);
    }

   
}
