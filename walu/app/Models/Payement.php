<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payement extends Model
{
    protected $fillable = [
        'reference', 'date_paiement', 'type_paiement', 'montant', 'url_facture', 'commande_id'
    ];

    public function commande(){
        return $this->belongsTo(Commande::class);
    }
}
