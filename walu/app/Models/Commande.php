<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    protected $fillable = [
        'reference', 'date_commande', 'date_livraison', 'url_commande', 'user_id'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function commandeProjet(){
        return $this->hasOne(CommandeProjet::class);
    }

    public function commandeProductions(){
        return $this->hasMany(CommandeProduction::class);
    }

    public function Productions(){
        return $this->belongsToMany(Production::class);
    }

    public function paiement(){
        return $this->hasOne(Paiement::class);
    }
}
