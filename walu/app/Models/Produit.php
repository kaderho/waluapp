<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    protected $fillable = [
        'designation', 'categorie_produit_id', 'slug', 'url_image', 'secteur_id'
    ];

    protected static function boot(){

        parent::boot();

        self::creating(function($model){

            $model->slug = str_slug($model->designation);
        });
    }

    public function categorieProduit(){
        return $this->belongsTo(CategorieProduit::class);
    }

    public function secteur(){
        return $this->belongsTo(Secteur::class);
    }

    public function projets(){
        return $this->hasMany(Projet::class);
    }

    public function productions(){
        return $this->hasMany(Production::class);
    }

    public function producteurs(){
        return $this->belongsToMany(Producteur::class, 'projets');
    }
}
