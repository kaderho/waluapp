<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ADMINISTRATEUR = 'administrateur';
    const MANAGER = 'manager';
    const PECHEUR = 'pecheur';
    const ELEVEUR = 'eleveur';
    const AGRICULTEUR = 'agriculteur';
    const ACHETEUR = 'acheteur';
    const INVESTISSEUR = 'investisseur';

    public function users(){

        return $this->hasMany(User::class);
    }
}
