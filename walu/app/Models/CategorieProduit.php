<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategorieProduit extends Model
{
    protected $fillable = [
        'nom', 'slug', 'url_image'
    ];

    protected static function boot()
    {
        parent::boot();

        self::creating(function($model){

            $model->slug = str_slug($model->nom);
        });
    }
    
    public function produits(){
        return $this->hasMany(Produit::class);
    }
}
