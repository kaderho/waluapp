<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ressource extends Model
{
    protected $fillable = [
        'libelle', 'quantite', 'montant', 'user_id', 'slug', 'preuve_url'
    ];

    protected static function boot(){

        parent::boot();

        self::creating(function($model){

            $model->slug = str_slug($model->libelle);

            if (auth()->check()) {
                
                $model->user_id = auth()->user()->id;
            }
        });
    }
    public function projet(){
        return $this->belongsTo(Projet::class);
    }

    public function user(){
        return $this->belongsTo(Projet::class);
    }
}
