<?php

namespace App;

use App\Models\Production;
use App\Models\Projet;
use App\Models\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;

    const ACTIF = 'actif';
    const INACTIF = 'inactif';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'adresse', 'telephone','password', 
        'email', 'code', 'role_id', 'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected static function boot(){

        parent::boot();

        self::creating(function($model){

                $token = bcrypt(str_random(60));
            $model->token = str_replace('/', '$', $token);

            $model->code = str_random(8);
        });
    }
    public function setPasswordAttribute($password){

        $this->attributes['password'] = Hash::make($password);
    }

    public function projets(){

        return $this->hasMany(Projet::class);
    }

    public function profil()
    {
        return $this->hasOne('App\Profil');
    }

    public function productions(){

        return $this->hasMany(Production::class);
    }

    public function role(){

        return $this->belongsTo(Role::class);
    }

    public function hasRole($role){

        if (is_string($role)) {
            return $this->role()->pluck('slug')->contains($role);
        }
        foreach ($role as $r) {
            if ($this->hasRole($r)) {
                return true;
            }
        }
    }

    public function isAdministrateur(){

        return $this->role->slug == Role::ADMINISTRATEUR;
    }

    public function isAgriculteur()
    {

        return $this->role->slug == Role::AGRICULTEUR;
    }

    public function isEleveur()
    {

        return $this->role->slug == Role::ELEVEUR;
    }

    public function isAcheteur()
    {

        return $this->role->slug == Role::ACHETEUR;
    }

    public function isInvestisseur()
    {

        return $this->role->slug == Role::INVESTISSEUR;
    }

    public function isPecheur()
    {

        return $this->role->slug == Role::PECHEUR;
    }

    public function isProducteur()
    {
        return in_array($this->role->slug, [
            Role::AGRICULTEUR,
            Role::PECHEUR,
            Role::ELEVEUR,
        ]);
    }
    
    public function isOwners()
    {
        return in_array($this->role->slug, [
            Role::ADMINISTRATEUR,
            Role::MANAGER,
        ]);
    }

    public function isManager()
    {

        return $this->role->slug == Role::MANAGER;
    }
}
