<?php

function set_active($resource)
{
    return strpos(request()->route()->uri(), $resource) !== false ? 'active' : '';
}
