<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profil extends Model
{
    
    protected $fillable = ['description', 'user_id', 'avatar'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

}
