<?php

namespace App\Providers;

use App\Models\CategorieProduit;
use App\Models\Produit;
use App\Models\Secteur;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
       
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer([
            'admin.produits.index',
        ], function ($view) {

            $view->with('catProduits', CategorieProduit::select('id', 'nom', 'slug')->orderBy('nom')->get());
        });

        view()->composer([
            'admin.projets.create', 'producteurs.projets.create', 'producteurs.productions.create'
        ], function ($view) {

            $view->with('produits', Produit::orderBy('designation')->get());
        });


        view()->composer([
            'homes.achats.index', 'admin.produits.index', 
        ], function ($view) {

            $view->with([
                'agricole' => Secteur::where('slug', 'agricole')->first(),
                'elevage' => Secteur::where('slug', 'elevage')->first(),
                'halieutique' => Secteur::where('slug', 'halieutique')->first(),
                'secteurs' => Secteur::all(),
            ]);
        });
    }
}
