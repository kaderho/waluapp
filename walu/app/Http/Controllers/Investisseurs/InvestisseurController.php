<?php

namespace App\Http\Controllers\Investisseurs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InvestisseurController extends Controller
{
    public function index(){

        $user = auth()->user();

        return view('admin.index')->with([
            'user' => $user,
        ]);
    }
}
