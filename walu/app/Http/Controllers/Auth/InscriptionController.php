<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\RegisterMail;
use App\Upload\FilesUploader;
use App\User;
use Illuminate\Support\Facades\Mail;

class InscriptionController extends Controller
{
    public function create(){

        return view('auth.register');
    }

    public function store(Request $request){

        $user = User::create($request->all());

        $user->profil()->create(['description' => 'en attente']);

        Mail::to($user)->send(new RegisterMail($user));
        
        return redirect()->route('connexion');
    }

}
