<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;

class ConnexionController extends Controller
{
    public function create()
    {

        return view('auth.login')->with([
            'banner' => 'Connexion'
        ]);
    }

    public function store(Request $request)
    {
        $request->validate = [
            'email' => 'bail|required',
            'password' => 'bail|required',
        ];

        //search user email
        $user = User::where('email', $request->email)->first();

        //test if user exist
        if ($user) {

            if ($user->status == User::INACTIF) {

                return back()->with([
                    'baner' => 'Connexion',
                    'title' => 'Compte non validé',
                    'orange' => "<b>$user->name </b>Votre compte n'a pas encore été validé !",
                ]);
            }
            //if user exist ok, test if, password hash checked the password entered in view 
            if (Hash::check($request->password, $user->password)) {
                //if passwords checked, us test if user checked the checkbox remember me
                if ($request->remember) {
                    //if ok, us storage the informations user in session
                    auth()->login($user, true);

                    //after he redirected on page where, he connected previoulsy
                    return redirect($request->redirect);
                }

                //esle us not storage the user in session
                auth()->login($user);

                //after he redirected on page where, he connected previoulsy
                return redirect($request->redirect);
            }
            return back()->with([
                'baner' => 'Connexion',
                'title' => "Oups...",
                'red' => "Mot de passe ou Email incorrecte",
            ]);
        }

        return back()->with([
            'banner' => 'Connexion',
            'title' => "Oups...",
            'red' => "Mot de passe ou Email incorrecte",
        ]);
    }

    public function validation(User $user, $token)
    {
        if ($user->exists) {
            if ($user->token === $token) {
                if ($user->status == User::INACTIF) {
                    $user->update([
                        'token' => null,
                        'status' => User::ACTIF,
                    ]);
                }
                return redirect('/connexion')->with([
                    'banner' => 'Connexion',
                    'title' => "Compte validé",
                    'green' => "Félicitation, <b>$user->name </b> votre compte à bien été validé !",
                ]);
            }

            return back()->with([
                'banner' => 'Connexion',
                'title' => "Oups...",
                'red' => "Désolé, il y a eu un petit problème !",
            ]);
        }
        return back()->with([
            'banner' => 'Connexion',
            'title' => "Utilisateur inexistant",
            'red' => "Désolé, je n'ai pas trouvé un utilisateur avec ce nom <b>$user->name </b>.",
        ]);
    }

    public function destroy()
    {
        $user = auth()->user();

        auth()->logout();

        return redirect()->route('connexion')->with([
            'title' => 'Déconnexion',
            'blue' => "<b>Ousp ! $user->name </b>, vous êtes déconnecté(e). A très bientôt !",
        ]);;
    }
}
