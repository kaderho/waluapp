<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Gloudemans\Shoppingcart\Facades\Cart;

class PanierController extends Controller
{
    public function index(){

        return view('homes.achats.panier');
    }

    public function store(Request $request)
    {
        $duplicate = Cart::search(function($cartProduit, $rowId) use ($request){
            return $cartProduit->id === $request->id;
        });
        if ($duplicate->isNotEmpty()) {
            
            return redirect()->route('panier')->withSuccess('Ce produit existe déjà dans votre panier !');
        }
        Cart::add([
            'id' => $request->id, 
            'name' => $request->nom, 
            'qty' => 1, 
            'price' => $request->prix,
            'weight' => $request->poids,
            ['size' => $request->taille]
            ])->associate('App\Models\Production');
        return back()->withSuccess('Vous avez ajouté un produit au panier');
    }

    public function update(Request $request, $rowId)
    {
        $validate = Validator::make($request->all(), ['quantity' => 'required|numeric|between:1,10']);
        if($validate->fails()){
            session()->flash('error', 'La quantité doit être inférieur à 11.');
            return response()->json(['success' => false], 500);
        }
        Cart::update($rowId, $request->quantity);
       session()->flash('success', 'La quantité du produit mise à jour !');
        return response()->json(['success' => true]);
    }

    public function destroy($rowId)
    {
        Cart::remove($rowId);
        return back()->withSuccess('Un produit a été supprimé du panier !');
    }

    public function empty(){

        Cart::destroy();
        return redirect()->route('panier')->withSuccess('Parnier vidé !');
    }
}
