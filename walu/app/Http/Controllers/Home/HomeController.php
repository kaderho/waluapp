<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;
use App\User;

class HomeController extends Controller
{
    public function __construct()
    {
    }
    public function home(){

        $producteurs = Role::whereIn('slug', ['eleveur', 'pecheur', 'agriculteur'])->pluck('id');

        $users = User::with('profil')->whereIn('role_id', $producteurs)->get();

        return view('homes.home')->with([
            'users' => $users,
        ]);
    }

    public function projet(){

        return view('homes.projet');
    }

    public function contact(){

        return view('homes.home');
    }

    public function apropos(){

        return view('homes.home');
    }

    public function user(){

        $user = auth()->user();

        if ($user->isOwners()) {
            
            return redirect()->route('admin.index');
        }
        
        if ($user->isProducteur()) {
            
            return redirect()->route('producteur.index');
        }

        if ($user->isInvestisseur()) {
            
            return redirect()->route('investisseur.index');
        }

        if ($user->isInvestisseur()) {
            
            return redirect()->route('investisseur.index');
        }

        if ($user->isAcheteur()) {
            
            return redirect()->route('acheteur.index');
        }
    }

    
}
