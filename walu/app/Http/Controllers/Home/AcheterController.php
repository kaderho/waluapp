<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CategorieProduit;
use App\Models\Production;
use Illuminate\Support\Facades\DB;

class AcheterController extends Controller
{
    public function index(){

        $rand = rand(1, 7);

        $catProduit = CategorieProduit::where('id', $rand)->first();

        $catProduitsG = DB::table('categorie_produits')
                ->inRandomOrder()
                ->limit(2)
                ->get();
        
        $catProduitsD = DB::table('categorie_produits')
                ->inRandomOrder()
                ->limit(2)
                ->get();

        $productions = Production::where('status', 'en cours')->get();

        $productionsA = Production::where('secteur_id', 1)->inRandomOrder()->take(8)->get();
        $productionsH = Production::where('secteur_id', 3)->inRandomOrder()->take(8)->get();
        $productionsE = Production::where('secteur_id', 2)->inRandomOrder()->take(8)->get();

        return view('homes.achats.index')->with([
            'catProduit' => $catProduit,
            'catProduitsG' => $catProduitsG,
            'catProduitsD' => $catProduitsD,
            'productionsA' => $productionsA,
            'productionsH' => $productionsH,
            'productionsE' => $productionsE,
        ]);
    }

    public function show($slug){

        $production = Production::where('slug', $slug)->first();

        $productions = Production::where('secteur_id', $production->secteur_id);

        return view('homes.achats.show')->with([
            'production' => $production,
            'productions' => $productions,
           
        ]);
    }

}
