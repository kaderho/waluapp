<?php

namespace App\Http\Controllers\Acheteurs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AcheteurController extends Controller
{
    public function index(){

        $user = auth()->user();

        return view('admin.index')->with([
            'user' => $user,
        ]);
    }
}
