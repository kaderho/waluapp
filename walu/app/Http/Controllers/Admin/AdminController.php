<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Commande;
use App\Models\CommandeProduction;
use App\Models\Production;
use App\Models\Projet;
use App\Models\Role;
use App\User;

class AdminController extends Controller
{
    public function index(){

        $user = auth()->user();

        $roles = Role::whereIn('slug', ['eleveur', 'pecheur', 'agriculteur'])->pluck('id');

        $countProducteur = User::whereIn('role_id', $roles)->get()->count();

        $countUser = User::whereNotIn('role_id', $roles)->get()->count();

        $countCmde = Commande::all()->count();

        $productionTotale = Production::all()->sum('quantite');

        $cmdeProduction = CommandeProduction::all()->sum('quantite');

        $percent = $cmdeProduction > 0 ? ($cmdeProduction / $productionTotale) * 100 : 100;

        return view('admin.index')->with([
            'user' => $user,
            'countProducteur'=> $countProducteur,
            'countuser' => $countUser,
            'countCmde'=> $countCmde,
            'percent' => $percent,
            'productionTotale' => $productionTotale,
            'cmdeProdction' => $cmdeProduction,
        ]);
    }

    public function user(Request $request){

        if ($request->user == Role::AGRICULTEUR) {
            
            $status = Role::AGRICULTEUR;

            $role = Role::where('slug', Role::AGRICULTEUR)->first();
            $users = $role->users()->get();

            // $users = User::where('role_id', $role->id)->get();
        }

        if ($request->user == Role::PECHEUR) {
            
            $status = Role::PECHEUR;
            $role = Role::where('slug', Role::PECHEUR)->first();
            $users = $role->users()->get();
        }

        if ($request->user == Role::ELEVEUR) {
            
            $status = Role::ELEVEUR;
            $role = Role::where('slug', Role::ELEVEUR)->first();
            $users = $role->users()->get();
        }

        if ($request->user == Role::ACHETEUR) {
            
            $status = Role::ACHETEUR;
            $role = Role::where('slug', Role::ACHETEUR)->first();
            $users = $role->users()->get();
        }

        if ($request->user == Role::INVESTISSEUR) {
            
            $status = Role::INVESTISSEUR;
            $role = Role::where('slug', Role::INVESTISSEUR)->first();
            $users = $role->users()->get();
        }

        return view('users.index')->with([
            'users' => $users,
            'status' => $status
        ]);
    }

    public function projet(Request $request){

        if ($request->projet == Projet::EN_ATTENTE) {
            
            $status = Projet::EN_ATTENTE;

            $projets = Projet::where('status', Projet::EN_ATTENTE)->get();
        }
        
        if ($request->projet == Projet::EN_COURS) {
            
            $status = Projet::EN_COURS;

            $projets = Projet::where('status', Projet::EN_COURS)->get();
        } 
        
        if ($request->projet == Projet::TERMINE) {
            
            $status = Projet::TERMINE;

            $projets = Projet::where('status', Projet::TERMINE)->get();
        } 

        return view('projets.index')->with([
            'projets' => $projets,
            'status' => $status
        ]);
    }
}
