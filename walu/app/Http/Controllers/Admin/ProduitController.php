<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Produit;
use Illuminate\Support\Facades\Storage;

class ProduitController extends Controller
{
    public function index(){

        $produits = Produit::orderBy('designation')->get();

        return view('admin.produits.index')->with([
            'produits' => $produits,
        ]);
    }

    public function store(Request $request){

        $file = $request->image;
        $fileExtension = strtolower($file->getClientOriginalExtension());

        if (in_array($fileExtension, ['jpg', 'jpeg', 'png'])) {
            $path = $file->store('imagesProduits', 'public');

            Produit::create([
                'designation' => $request->designation,
                'categorie_produit_id' => $request->categorie_produit_id,
                'url_image' => $path,
                'secteur_id' => $request->secteur_id]);
    
            return  redirect()->route('produit.index');
        }
        else{
           return back();
        }

    }

    public function destroy($slug){

        $produit = Produit::where('slug', $slug)->firstOrFail();
        
        //$file = Storage::disk('public')->exists($produit->photo)->get($produit->photo);
        $file = Storage::disk('public')->delete($produit->image_url);       
        
        $produit->delete();

        return back();
    }
}
