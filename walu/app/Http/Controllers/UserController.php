<?php

namespace App\Http\Controllers;

use App\Profil;
use App\Upload\FilesUploader;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(){

        $user = auth()->user();

        if ($user->isOwners()) {
            
            return redirect()->route('admin.index');
        }
        
        if ($user->isProducteur()) {
            
            return redirect()->route('producteur.index');
        }

        if ($user->isInvestisseur()) {
            
            return redirect()->route('investisseur.index');
        }

        if ($user->isInvestisseur()) {
            
            return redirect()->route('investisseur.index');
        }

        if ($user->isAcheteur()) {
            
            return redirect()->route('acheteur.index');
        }
    }

    public function profileIndex(User $user){


        return view('users.profile')->with([
            'user' => $user,
        ]);
    }

    public function profileUpdate(Request $request, User $user)
    {
        $userData = [];

        if ($user->name != $request->name) { array_push($userData, ['name' => $request->name]);}
        if ($user->adresse != $request->adresse) {array_push($userData, ['adresse' => $request->adresse]);}
        if ($user->telephone != $request->telephone) {array_push($userData, ['telephone' => $request->telephone]);}
        if ($user->password != $request->password) {array_push($userData, ['password' => $request->password]);}
        if ($user->email != $request->email) {array_push($userData, ['email' => $request->email]);}

        $user->update($userData);

        $path = FilesUploader::storeFIles($request->image);

        if ($user->profil == null) {
            
            $user->profil()->create(['avatar' => $path, 'description' => $request->description]);
        }
        else{
            $profilData = [];

            if ($user->profil->avatar != $path) {array_push($profilData, ['avatar' => $path]);}
            if ($user->description != $request->description) {array_push($profilData, ['avatar' => $path]);}
      
        $user->profil()->update($profilData);

        }
        return view('users.profile')->with([
            'user' => $user,
            'title' => 'Profile mis à jour !',
            'blue' => 'Félicitation ! Votre profile a été mis à jour avec succès les visiteurs seront très ravis.',
        ]);
    }
}
