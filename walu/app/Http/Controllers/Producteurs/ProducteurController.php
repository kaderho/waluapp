<?php

namespace App\Http\Controllers\Producteurs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Projet;

class ProducteurController extends Controller
{
    public function index(){

        $user = auth()->user();

        return view('producteurs.index')->with([
            'user' => $user,
        ]);
    }

    
}
