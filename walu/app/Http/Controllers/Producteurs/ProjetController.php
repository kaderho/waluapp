<?php

namespace App\Http\Controllers\Producteurs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Projet;

class ProjetController extends Controller
{
    public function index(Request $request)
    {

        $user = auth()->user();

        if ($request->status == Projet::EN_ATTENTE) {

            $status = Projet::EN_ATTENTE;

            $projets = $user->projets()->where('status', Projet::EN_ATTENTE)->get();

            // $projets = Projet::where('status', Projet::EN_ATTENTE)->get();
        }

        if ($request->status == Projet::EN_COURS) {

            $status = Projet::EN_COURS;

            $projets = $user->projets()->where('status', Projet::EN_COURS)->get();
            // $projets = Projet::where('status', Projet::EN_COURS)->get();
        }

        if ($request->status == Projet::TERMINE) {

            $status = Projet::TERMINE;

            $projets = $user->projets()->where('status', Projet::TERMINE)->get();
            // $projets = Projet::where('status', Projet::TERMINE)->get();
        }

        return view('projets.index')->with([
            'projets' => $projets,
            'status' => $status
        ]);
    }

    public function create()
    {

        return view('producteurs.projets.create');
    }

    public function store(Request $request)
    {
        if ($request->date_debut) {
            
            $projet = Projet::create($request->all());
            $projet->update(['status' => 'en cours']);
        }
        else{

        $projet = Projet::create($request->all());
    }

        return redirect()->route('ressource.create', ['slug'=>$projet->slug]);
    }
    
    public function destroy($slug)
    {
        $projet = Projet::where('slug', $slug)->firstOrFail();
        
        $projet->delete();

        return back();
    }
}
