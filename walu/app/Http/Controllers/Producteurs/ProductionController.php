<?php

namespace App\Http\Controllers\Producteurs;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Production;
use App\Models\Projet;

class ProductionController extends Controller
{
    public function index(Request $request)
    {

        $user = auth()->user();

        if ($request->status == Production::EN_COURS) {

            $status = Production::EN_COURS;

            $productions = $user->productions()->where('status', Production::EN_COURS)->get();
            // $productions = Production::where('status', Production::EN_COURS)->get();
        }

        if ($request->status == Production::TERMINE) {

            $status = Production::TERMINE;

            $productions = $user->productions()->where('status', Production::TERMINE)->get();
            // $productions = Production::where('status', Production::TERMINE)->get();
        }

        return view('producteurs.productions.index')->with([
            'productions' => $productions,
            'status' => $status
        ]);
    }

    public function create(){

        $user = auth()->user();

        $projets = $user->projets()->where('status', Projet::EN_COURS)->get();
        return view('producteurs.productions.create')->with([

            'projets' => $projets,
        ]);
    }

    public function store(Request $request){

        $user = auth()->user();

        if ($request->image) {
            
            $file = $request->image;
            $fileExtension = strtolower($file->getClientOriginalExtension());
    
            if (in_array($fileExtension, ['jpg', 'jpeg', 'png'])) {
                $path = $file->store('imagesProduction', 'public');
                
                $production = $user->productions()->create(array_merge($request->all(), ['url_image' => $path]));

            }
        }
        else {
            
        $production = $user->productions()->create($request->all());
    }

        
        return redirect()->route('production',['status' => 'en cours']);

    }

}
