<?php

namespace App\Http\Controllers\Producteurs;

use App\Upload\FilesUploader;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Projet;
use App\Models\Ressource;
use Illuminate\Support\Facades\Storage;

class RessourceController extends Controller
{
    public function index(){

        $ressources = Ressource::all();

        return view('producteurs.ressources.index')->with([
            'ressources' => $ressources,
        ]);
    }

    public function create(){

        return view('producteurs.ressources.create')->with([
        ]);
    }

    public function store(Request $request){
        
        $path = FilesUploader::storeFIles($request->image);

        $ressource = Ressource::create(array_merge(['preuve_url' => $path], $request->all()));
        return redirect()->route('ressource')->with([
            'title' => 'Nouvelle ajout réussie.',
            'green' => "Félicitation! Vous avez ajouté <strong>$ressource->libelle</strong>."
        ]);

    }

    public function edit($slug)
    {
        $ressource = Ressource::where('slug', $slug)->first();
        return view('producteurs.ressources.edit')->withRessource($ressource);
    }

    public function update(Request $request, $slug)
    {
        $ressource = Ressource::where('slug', $slug)->first();
        $path = $request->hasfile('image') ? FilesUploader::storeFIles($request->image): $ressource->preuve_url;

        $ressource->update(array_merge(['preuve_url' => $path], $request->all()));
        
        return redirect()->route('ressource')->with([
            'title' => 'modification réussie.',
            'green' => "Félicitation! Vous avez modifié la ressource."
        ]);
    }

    public function destroy($slug)
    {

        $ressource = Ressource::where('slug', $slug)->first();
        Storage::delete($ressource->preuve_url);
        $ressource->delete();
        return redirect()->route('ressource')->with([
            'title' => 'modification réussie.',
            'green' => "Ressource <strong>$ressource->preuve_url</strong> supprimée avec succès."
        ]);
    }
}
