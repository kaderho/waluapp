<?php

namespace App\Upload;

class FilesUploader
{
    public static function storeFIles($file){
        $fileExtension = strtolower($file->getClientOriginalExtension());
        if (in_array($fileExtension, ['jpg', 'jpeg', 'png'])) {
            $path = $file->store('imageFile', 'public');
            
            return $path;
        }
        if(in_array($fileExtension, ['xlsx'])){
            $filename = $file->getClientOriginalName();
            $file->storeAs('excelFile',$file->getClientOriginalName(), 'public');
            
            return $filename;
        }
        if(in_array($fileExtension, ['pdf'])){
            $filename = $file->getClientOriginalName();
            $file->storeAs('filePdf',$file->getClientOriginalName(), 'public');
            
            return $filename;
        }
    }

}

